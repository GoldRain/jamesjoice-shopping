package eu.contat.jamesjoice;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Arrays;

import eu.contat.jamesjoice.main.MenuActivity;
import eu.contat.jamesjoice.model.JSendResponse;
import eu.contat.jamesjoice.model.LoginResponse;
import eu.contat.jamesjoice.worker.LoginWorker;
import rebus.permissionutils.PermissionEnum;
import rebus.permissionutils.PermissionManager;
import rebus.permissionutils.SmartCallback;

public class LoginActivity extends AppCompatActivity implements Response.Listener<String>, Response.ErrorListener, GoogleApiClient.OnConnectionFailedListener {


    private GoogleApiClient mGoogleApiClient;
    public static CallbackManager callbackManager;
    public static final int GOOGLE_RC = 9001;
    public static final int FACEBOOK_RC = 64206;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //social login
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this/* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        final TextInputLayout usernameTextObj = (TextInputLayout) findViewById(R.id.user);
        final TextInputLayout passTextObj = (TextInputLayout) findViewById(R.id.password);
        Typeface font = Typeface.createFromAsset(getAssets(), "font/berlin_sans_fb_regular.ttf");
        usernameTextObj.setTypeface(font);
        passTextObj.setTypeface(font);
        TextView frogot = (TextView) findViewById(R.id.forgot);
        frogot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, ForgotPasswordActivity.class));
            }
        });

        Button b = (Button) findViewById(R.id.mailBtn);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
            }
        });

        Button login = (Button) findViewById(R.id.loginBtn);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, MenuActivity.class));
                new LoginWorker().login(
                        LoginActivity.this,
                        LoginActivity.this,
                        usernameTextObj.getEditText().getText().toString(),
                        passTextObj.getEditText().getText().toString()
                );

            }
        });

        Button btn_fb_signin = (Button)findViewById(R.id.btn_fb_signin);
        btn_fb_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                facebookSignIn();
            }
        });

        Button btn_google_signin = (Button)findViewById(R.id.btn_google_signin);
        btn_google_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                googleSignIn();
            }
        });


    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onResponse(String response) {

        ObjectMapper mapper = JJApplication.getInstance().getObjectMapper();
        try {
            JSendResponse<LoginResponse> jsonResponse = mapper.readValue(response, new TypeReference<JSendResponse<LoginResponse>>() {
            });
            if (jsonResponse.getStatus() != null && jsonResponse.getStatus().equals(JSendResponse.STATUS_success)) {
                startActivity(new Intent(this, MenuActivity.class));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void processSocialLogin(String email, String id, String name){

        Toast.makeText(LoginActivity.this, "Email : " + email + " Name : " + name, Toast.LENGTH_SHORT).show();
    }

    private void facebookSignIn(){

        callbackManager = CallbackManager.Factory.create();

        // set permissions
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email"/*, "user_photos"*/, "public_profile"));

        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                // Facebook Email address
                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {

                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {

                        try {

                            String userId = object.getString("id");
                            String email = object.getString("email");
                            String name = object.getString("name");
                            String photo_url = "http://graph.facebook.com/" + userId + "/picture?type=large";

                            processSocialLogin(email, userId, name);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

                Bundle parameters = new Bundle();
                parameters.putString("fields", "id, name,email, picture");
                request.setParameters(parameters);
                request.executeAsync();

            }

            @Override
            public void onCancel() {
                Log.d("fail", "fail");
                LoginManager.getInstance().logOut();
            }

            @Override
            public void onError(FacebookException error) {
                LoginManager.getInstance().logOut();
                Log.d("error==>", error.toString());
            }

        });

    }

    private void googleSignIn() {

        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, GOOGLE_RC);

    }

    // [START handleSignInResult]
    private void handleSignInResult(GoogleSignInResult result) {

        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();

            String userId = acct.getId();
            String email = acct.getEmail();
            String name = acct.getDisplayName();

            processSocialLogin(email, userId, name);

        } else {

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode){

            case GOOGLE_RC:
                GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
                handleSignInResult(result);

                break;

            case FACEBOOK_RC:
                callbackManager.onActivityResult(requestCode, resultCode, data);
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

}
