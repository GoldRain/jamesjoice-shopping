package eu.contat.jamesjoice.main;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.material.textfield.TextInputLayout;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.stripe.android.ApiResultCallback;
import com.stripe.android.PaymentConfiguration;
import com.stripe.android.PaymentIntentResult;
import com.stripe.android.Stripe;
import com.stripe.android.model.ConfirmPaymentIntentParams;
import com.stripe.android.model.PaymentIntent;
import com.stripe.android.model.PaymentMethodCreateParams;
import com.stripe.android.view.CardInputWidget;

import org.json.JSONException;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import eu.contat.jamesjoice.JJApplication;
import eu.contat.jamesjoice.R;
import eu.contat.jamesjoice.model.CheckoutData;
import eu.contat.jamesjoice.model.CheckoutField;
import eu.contat.jamesjoice.model.JSendResponse;
import eu.contat.jamesjoice.model.LoginResponse;
import eu.contat.jamesjoice.model.OrderResponse;
import eu.contat.jamesjoice.model.PaymentMethod;
import eu.contat.jamesjoice.model.Product;
import eu.contat.jamesjoice.model.ShippingMethod;
import eu.contat.jamesjoice.worker.ShoppingCartWorker;

import static eu.contat.jamesjoice.main.MenuActivity.PRODUCT_Q;
import static eu.contat.jamesjoice.main.MenuActivity.getProductQuantityKey;

public class ShoppingCartActivity extends AppCompatActivity implements Response.Listener<String>, Response.ErrorListener{

    public static final String PRODUCT_LIST_KEY = "cart_products", USER_DATA_KEY = "userDataPrefs";
    private static final String STRIPE_KEY = "stripeKey", PAYPAL_KEY = "paypalKey", STRIPE_CLIENT_SECRET_KEY = "stripeClientSecret";
    private static final int PAYPAL_REQUEST_CODE = 77;

    private List<Product> mProducts;
    private ProgressBar mProgressBar;
    private CheckoutData mCheckData;
    private NestedScrollView mScrollView;
    private TextView mSumLabel;
    private Map<CheckoutField, EditText> mInputSet = new HashMap<>();
    private PaymentMethod mPaymentMethod;
    private ShippingMethod mShippingMethod;
    private CardInputWidget mWidget;
    private Stripe stripe;
    private Set<CheckoutField> mFields = new HashSet<>();
    private View mGPSView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping_cart);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        mProgressBar = findViewById(R.id.progressbar);
        mSumLabel = findViewById(R.id.sum);

        LocalBroadcastManager.getInstance(this).registerReceiver(
                mMessageReceiver, new IntentFilter("GPSLocationUpdates"));

        Intent intent = getIntent();
        if(intent != null){
            List<Product> list = intent.getParcelableArrayListExtra(PRODUCT_LIST_KEY);
            mProducts = list;
            Collections.sort(mProducts);

            RecyclerView recyclerView = findViewById(R.id.recycle);
            Adapter adapter = new Adapter(mProducts);
            recyclerView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
            recyclerView.setNestedScrollingEnabled(false);
            checkout();
        }



        Button submitBtn = findViewById(R.id.submitBtn);
        submitBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                checkout(new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        ObjectMapper mapper = JJApplication.getInstance().getObjectMapper();
                        try {
                            JSendResponse<CheckoutData> jsonResponse = mapper.readValue(response, new TypeReference<JSendResponse<CheckoutData> >() {
                            });
                            if (jsonResponse.getStatus() != null && jsonResponse.getStatus().equals(JSendResponse.STATUS_success)) {
                                CheckoutData data = jsonResponse.getData();

                                if(mPaymentMethod == null || mShippingMethod == null){
                                    Toast.makeText(ShoppingCartActivity.this, "Selezionare un metodo di pagamento ed uno di spedizione.", Toast.LENGTH_LONG).show();
                                    return;
                                }

                                for(PaymentMethod pm : data.getPaymentMethods()){
                                    if(pm.getId() != null && pm.getId().equals("stripe")){
                                        Map<String,String> map = pm.getSettings();
                                        SharedPreferences.Editor editor = getPreferences(MODE_PRIVATE).edit();
                                        editor.putString(STRIPE_KEY, map.get("public_key"));
                                        editor.putString(STRIPE_CLIENT_SECRET_KEY, pm.getIntent());
                                        editor.commit();
                                    } else if(pm.getId() != null && pm.getId().equals("paypal")){
                                        Map<String,String> map = pm.getSettings();
                                        SharedPreferences.Editor editor = getPreferences(MODE_PRIVATE).edit();
                                        editor.putString(PAYPAL_KEY, map.get("client_id"));
                                        editor.commit();
                                    }
                                }

                                if(mPaymentMethod.getId() != null && mPaymentMethod.getId().equals("stripe")) {

                                    submitStripe();
                                }else if(mPaymentMethod.getId() != null && mPaymentMethod.getId().equals("paypal")){
                                    for(PaymentMethod pm : data.getPaymentMethods()) {
                                        if (pm.getId() != null && pm.getId().equals("paypal")) {
                                            Map<String,String> map = pm.getSettings();
                                            submitPaypal(pm.getCurrency(), pm.getDescription(), pm.getAmount());
                                        }
                                    }
                                }else{
                                    submit();
                                }
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }



                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });




            }
        });
    }

    @Override
    public void onDestroy() {
        stopService(new Intent(this, PayPalService.class));
        super.onDestroy();
    }

    private void submitPaypal(String currency, String descr, Double total){
        SharedPreferences prefs = getPreferences(MODE_PRIVATE);
        PayPalConfiguration config = new PayPalConfiguration()

                // Start with mock environment.  When ready, switch to sandbox (ENVIRONMENT_SANDBOX)
                // or live (ENVIRONMENT_PRODUCTION)
                .environment(PayPalConfiguration.ENVIRONMENT_NO_NETWORK)

                .clientId(prefs.getString(PAYPAL_KEY, ""));

        Intent intentP = new Intent(this, PayPalService.class);

        intentP.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);

        startService(intentP);

        PayPalPayment payment = new PayPalPayment(new BigDecimal(total), currency, descr,
                PayPalPayment.PAYMENT_INTENT_SALE);

        Intent intent = new Intent(this, PaymentActivity.class);

        // send the same configuration for restart resiliency
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);

        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payment);

        startActivityForResult(intent, PAYPAL_REQUEST_CODE);
    }

    private void submitStripe(){
        SharedPreferences prefs = getPreferences(MODE_PRIVATE);
        PaymentConfiguration.init(
                getApplicationContext(),
                prefs.getString(STRIPE_KEY, "")
        );

        PaymentMethodCreateParams params = mWidget.getPaymentMethodCreateParams();
        if (params != null && mPaymentMethod.getIntent() != null) {
            ConfirmPaymentIntentParams confirmParams = ConfirmPaymentIntentParams
                    .createWithPaymentMethodCreateParams(params, prefs.getString(STRIPE_CLIENT_SECRET_KEY, ""));
            final Context context = getApplicationContext();
            stripe = new Stripe(
                    context,
                    PaymentConfiguration.getInstance(context).getPublishableKey()
            );
            if (mPaymentMethod.getId() != null && mPaymentMethod.getId().equals("stripe")) {
                stripe.confirmPayment(ShoppingCartActivity.this, confirmParams);
                return;
            }

        }
    }

    private void checkout(){
        checkout(this, this);
    }

    private void checkout(Response.Listener<String> responseListener, Response.ErrorListener errorListener){


        mProgressBar.setVisibility(View.VISIBLE);
        SharedPreferences prefs = getSharedPreferences(PRODUCT_Q, Context.MODE_PRIVATE);
        LinkedHashMap<Long, Integer> map = new LinkedHashMap<>();
        double sum = 0;

        try {
            for (Product p : mProducts) {
                int q = prefs.getInt(MenuActivity.getProductQuantityKey(p), 0);
                if (q > 0) {
                    map.put(p.getId(), q);

                }
                Double price = Double.parseDouble(p.getRegular_price());
                sum += price * q;
            }
            NumberFormat format = new DecimalFormat("#.##");

            format.setMinimumFractionDigits(2);
            this.mSumLabel.setText("\u20ac" + format.format(sum));
            new ShoppingCartWorker().checkout(responseListener, errorListener, map);
        }catch(NumberFormatException e){
            e.printStackTrace();
        }
    }

    private void submit(){
        this.submit(Collections.EMPTY_MAP);
    }

    private void submit(Map<String, String> additionalParams){

        LinkedHashMap<Long, Integer> map = new LinkedHashMap<>();
        SharedPreferences prefs = getSharedPreferences(PRODUCT_Q, Context.MODE_PRIVATE);


        for (Product p : mProducts) {
            int q = prefs.getInt(MenuActivity.getProductQuantityKey(p), 0);
            if (q > 0) {
                map.put(p.getId(), q);

            }

        }
        final Map<String, List<String>> fieldParams = new HashMap<>();
        for(Map.Entry<CheckoutField, EditText> entry : mInputSet.entrySet()) {
            if (entry.getValue().getText() == null || entry.getValue().getText().length() == 0)
                continue;
                fieldParams.put(entry.getKey().getName(), Arrays.asList(entry.getValue().getText().toString()));
        }

        for(CheckoutField field : this.mFields){
            fieldParams.put(field.getName(), new ArrayList<String>(field.getValues()));
        }

        mProgressBar.setVisibility(View.VISIBLE);
        new ShoppingCartWorker().submitOrder(new Response.Listener<String>() {
                                                 @Override
                                                 public void onResponse(String response) {
                                                     ObjectMapper mapper = JJApplication.getInstance().getObjectMapper();
                                                     try {
                                                         JSendResponse<OrderResponse> jsonResponse = mapper.readValue(response, new TypeReference<JSendResponse<OrderResponse> >() {
                                                         });
                                                         if (jsonResponse.getStatus() != null && jsonResponse.getStatus().equals(JSendResponse.STATUS_success)) {

                                                             Toast.makeText(ShoppingCartActivity.this, "Ordine avvenuto con successo.", Toast.LENGTH_LONG).show();

                                                             SharedPreferences prefs = getSharedPreferences(USER_DATA_KEY, MODE_PRIVATE);
                                                             SharedPreferences.Editor editor = prefs.edit();
                                                             for(Map.Entry<CheckoutField, EditText> entry : mInputSet.entrySet())
                                                                 editor.putString(entry.getKey().getName(), entry.getValue().getText().toString());


                                                             editor.commit();


                           /*     Intent intent = new Intent(ShoppingCartActivity.this, OrderDetailActivity.class);
                                intent.putExtra(OrderDetailActivity.DATA_KEY, jsonResponse.getData());
                                startActivity(intent);
                                */

                                                         }
                                                     } catch (IOException e) {
                                                         e.printStackTrace();
                                                         Log.e("", "message", e);
                                                     }finally {
                                                         mProgressBar.setVisibility(View.GONE);
                                                     }
                                                 }
                                             }, new Response.ErrorListener() {
                                                 @Override
                                                 public void onErrorResponse(VolleyError error) {
                                                     Log.e("", "message", error);
                                                     Toast.makeText(ShoppingCartActivity.this, "Errore durante l'invio dell'ordine.", Toast.LENGTH_LONG).show();


                                                 }
                                             },
                map, mShippingMethod, mPaymentMethod ,fieldParams, 5l , additionalParams );
    }

    private void updateSum(){
        SharedPreferences prefs = getSharedPreferences(PRODUCT_Q, Context.MODE_PRIVATE);
        double sum = 0;
        try {
            for (Product p : mProducts) {
                int q = prefs.getInt(MenuActivity.getProductQuantityKey(p), 0);


                    Double price = Double.parseDouble(p.getRegular_price());
                    sum += price * q;

            }
            NumberFormat format = new DecimalFormat("#.##");

            format.setMinimumFractionDigits(2);
            this.mSumLabel.setText("\u20ac" + format.format(sum));

        }catch(NumberFormatException e){
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == PAYPAL_REQUEST_CODE){
        if (resultCode == Activity.RESULT_OK) {
            PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
            if (confirm != null) {
                try {
                    Log.i("paymentExample", confirm.toJSONObject().toString(4));
                    Map<String, String> map = new HashMap<>();
                    String format = "payment[paypal][%s]";
                    map.put(String.format(format, "id"), confirm.getProofOfPayment().getPaymentId());
                    map.put(String.format(format, "state"), confirm.getProofOfPayment().getState());
                    map.put(String.format(format, "intent"), confirm.getProofOfPayment().getIntent());

                    submit(map);
                    // TODO: send 'confirm' to your server for verification.
                    // see https://developer.paypal.com/webapps/developer/docs/integration/mobile/verify-mobile-payment/
                    // for more details.

                } catch (JSONException e) {
                    Log.e("paymentExample", "an extremely unlikely failure occurred: ", e);
                }
            }
        }
        else if (resultCode == Activity.RESULT_CANCELED) {
            Log.i("paymentExample", "The user canceled.");
        }
        else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
            Log.i("paymentExample", "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
        }
        }else {
            // Handle the result of stripe.confirmPayment
            stripe.onPaymentResult(requestCode, data, new PaymentResultCallback(this));
            return;
        }
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        if(item.getItemId() == android.R.id.home){
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        try{
            Log.e("", "error checking out",error);
        }finally{
            mProgressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void onResponse(String response) {
        ObjectMapper mapper = JJApplication.getInstance().getObjectMapper();
        try {
            JSendResponse<CheckoutData> jsonResponse = mapper.readValue(response, new TypeReference<JSendResponse<CheckoutData> >() {
            });
            if (jsonResponse.getStatus() != null && jsonResponse.getStatus().equals(JSendResponse.STATUS_success)) {
                this.mCheckData = jsonResponse.getData();
                populateForm();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            mProgressBar.setVisibility(View.GONE);
        }
    }

    private void populateForm(){
        if(this.mCheckData != null){
            LinearLayout superview = findViewById(R.id.formBody);
            final View paymentView ;
           f: if(this.mCheckData.getFields() != null && !mCheckData.getFields().isEmpty()){
                List<CheckoutField> fields = mCheckData.getFields().get(0);
                if(fields.isEmpty())
                    break f;
                View view = getLayoutInflater().inflate(R.layout.shopping_cart_form_layout, superview, false);
                TextView title = view.findViewById(R.id.title);
                title.setText("Dati Personali");
                LinearLayout container = view.findViewById(R.id.body);
                for(final CheckoutField field : fields){
                    if(field.getType() != null && field.getType().equals("text")){
                        View subView = getLayoutInflater().inflate(R.layout.checkout_field, container, false);
                        TextInputLayout inputLayout = subView.findViewById(R.id.text);
                        inputLayout.setHint(field.getLabel());
                        inputLayout.getEditText().getText().clear();
                        inputLayout.getEditText().setTag(field.getValue());
                        Log.d("ShoppingCart", "string: " + getSharedPreferences(USER_DATA_KEY,MODE_PRIVATE).getString(field.getValue(), ""));
                        inputLayout.getEditText().setText(getSharedPreferences(USER_DATA_KEY,MODE_PRIVATE).getString(field.getValue(), ""));

                        mInputSet.put(field, inputLayout.getEditText());
                        container.addView(subView);
                    }else if(field.getType() != null && field.getType().equals("textarea")){
                        View subView = getLayoutInflater().inflate(R.layout.checkout_textarea, container, false);
                        TextInputLayout inputLayout = subView.findViewById(R.id.text);
                        inputLayout.setHint(field.getLabel());
                        inputLayout.getEditText().getText().clear();
                        inputLayout.getEditText().setTag(field.getValue());
                        Log.d("ShoppingCart", "string: " + getSharedPreferences(USER_DATA_KEY,MODE_PRIVATE).getString(field.getValue(), ""));
                        inputLayout.getEditText().setText(getSharedPreferences(USER_DATA_KEY,MODE_PRIVATE).getString(field.getValue(), ""));

                        mInputSet.put(field, inputLayout.getEditText());
                        container.addView(subView);
                    }else if(field.getType() != null && field.getType().equals("radio")){
                        View subView = getLayoutInflater().inflate(R.layout.shopping_cart_select_field, container, false);
                        RadioGroup group = subView.findViewById(R.id.rg);
                        TextView textLabel = subView.findViewById(R.id.text);
                        textLabel.setVisibility(View.VISIBLE);
                        textLabel.setText(field.getLabel());
                        this.mFields.add(field);
                        for(final String key : field.getSelect().keySet()){
                            final RadioButton button = new RadioButton(this);
                            button.setText(field.getSelect().get(key));
                            button.setTextColor(ContextCompat.getColor(this, R.color.contentText));
                            button.setTag(key);
                            int[][] states = new int[][] {
                                    new int[] {-android.R.attr.state_checked}, // disabled
                                    new int[] { android.R.attr.state_checked}, // unchecked
                            };

                            int[] colors = new int[] {
                                    ContextCompat.getColor(this, R.color.contentText),
                                    ContextCompat.getColor(this, R.color.jOrange)

                            };

                            ColorStateList cl = new ColorStateList(states, colors);
                            button.setButtonTintList(cl);
                            group.addView(button);

                            button.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    if(button.isSelected()){
                                        field.getValues().clear();
                                        field.getValues().add(field.getSelect().get(key));
                                    }
                                }
                            });

                        }
                        container.addView(subView);
                    }else if(field.getType() != null && field.getType().equals("checkbox")){
                        final Set<CheckBox> boxes = new HashSet<>();
                        View subview = getLayoutInflater().inflate(R.layout.shopping_cart_form_layout_1, container, false);
                        subview.setElevation(0);
                        TextView textTitle = subview.findViewById(R.id.text);
                        textTitle.setVisibility(View.VISIBLE);
                        textTitle.setText(field.getLabel());
                        LinearLayout body = subview.findViewById(R.id.body);
                        final LinearLayout subContainer = subview.findViewById(R.id.fields);
                        subContainer.setVisibility(View.GONE);
                        this.mFields.add(field);
                      //  subContainers.add(subContainer);
                        for(final String key : field.getSelect().keySet()) {
                            View labelView = getLayoutInflater().inflate(R.layout.checkout_label, container, false);
                            TextView name = labelView.findViewById(android.R.id.text1);
                            name.setText(field.getSelect().get(key));
                            final CheckBox box = labelView.findViewById(R.id.check);
                            boxes.add(box);

                            box.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                                    if (b) {

                                        field.getValues().add(field.getSelect().get(key));
                                    }else{
                                        field.getValues().remove(field.getSelect().get(key));
                                    }
                                }
                            });
                            body.addView(labelView);
                        }
                        container.addView(subview);
                    }
                }

                superview.addView(view);
            }

            if(mCheckData.getPaymentMethods() != null && !mCheckData.getPaymentMethods().isEmpty()){

                View view = getLayoutInflater().inflate(R.layout.shopping_cart_form_layout, superview, false);
                paymentView = view;
                view.setVisibility(View.GONE);
                TextView title = view.findViewById(R.id.title);
                title.setText("Metodo Di Pagamento");
                LinearLayout container = view.findViewById(R.id.body);
                final Set<CheckBox> boxes = new HashSet<>();
                for(final PaymentMethod pm : mCheckData.getPaymentMethods()){
                    View labelView = getLayoutInflater().inflate(R.layout.checkout_label, container, false);
                    TextView name = labelView.findViewById(android.R.id.text1);
                    name.setText( pm.getName());
                    container.addView(labelView);
                    final CheckBox box = labelView.findViewById(R.id.check);
                    boxes.add(box);

                    if(pm.getId() != null && pm.getId().equals("paypal")){
                        name.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(this, R.drawable.ic_resources_icons_paypal), null);

                    }
                    final View payView ;
                    if(pm.getId() != null && pm.getId().equals("stripe")){
                        name.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(this, R.drawable.ic_resources_icons_credit_card), null);


                        View pmView = getLayoutInflater().inflate(R.layout.shopping_cart_payment_input, container, false);
                        payView = pmView;
                        pmView.setVisibility(View.GONE);
                        CardInputWidget widget = pmView.findViewById(R.id.stripe);
                        this.mWidget = widget;
                        container.addView(pmView);
                    }else{
                        payView = new View(this);
                    }

                    box.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                            payView.setVisibility( b ? View.VISIBLE : View.GONE);
                            if(b){
                                mPaymentMethod = pm;
                                for(CheckBox check : boxes){
                                    if(!check.equals(box)){
                                        check.setChecked(false);
                                    }
                                }
                            }
                        }
                    });
                }


            }else{
                paymentView = new View(this);
            }

            if(mCheckData.getShippingMethods() != null && !mCheckData.getShippingMethods().isEmpty()){
                List<ShippingMethod> list = mCheckData.getShippingMethods();

                View view = getLayoutInflater().inflate(R.layout.shopping_cart_form_layout, superview, false);
                TextView title = view.findViewById(R.id.title);
                title.setText("Metodo D'Ordine");
                view.findViewById(R.id.body).setVisibility(View.GONE);
                final LinearLayout container = view.findViewById(R.id.body2);
                container.setVisibility(View.VISIBLE);
             //   container.setPadding(0,4,0,4);
             //   container.setBackground(null);
                final Set<LinearLayout> subContainers = new HashSet<>();
                final Set<CheckBox> boxes = new HashSet<>();
                for(final ShippingMethod sm : list){
                    View subview = getLayoutInflater().inflate(R.layout.shopping_cart_form_layout_1, container, false);
                    LinearLayout body = subview.findViewById(R.id.body);
                    final LinearLayout subContainer = subview.findViewById(R.id.fields);
                    subContainer.setVisibility(View.GONE);
                    subContainers.add(subContainer);
                    View labelView = getLayoutInflater().inflate(R.layout.checkout_label, container, false);
                    TextView name = labelView.findViewById(android.R.id.text1);
                    name.setText(sm.getName());
                    final CheckBox box = labelView.findViewById(R.id.check);
                    boxes.add(box);

                    box.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                            if(b){
                                mShippingMethod = sm;
                                paymentView.setVisibility(View.VISIBLE);
                                subContainer.setVisibility(View.VISIBLE);
                                for(CheckBox check : boxes){
                                    if(!check.equals(box)){
                                        check.setChecked(false);
                                    }
                                }
                                for(View v : subContainers){
                                    if(!v.equals(subContainer)){
                                        v.setVisibility(View.GONE);
                                    }
                                }
                            }else{
                                subContainer.setVisibility(View.GONE);
                            }
                        }
                    });

                    body.addView(labelView);

                    for(CheckoutField field : sm.getFields()){
                        if("text".equals(field.getType())){
                            View subView = getLayoutInflater().inflate(R.layout.checkout_field, subContainer, false);
                            TextInputLayout inputLayout = subView.findViewById(R.id.text);
                            inputLayout.setHint(field.getLabel());
                            inputLayout.getEditText().getText().clear();
                            //  inputLayout.getEditText().getText().clear();
                            subContainer.addView(subView);
                        }else if("select".equals(field.getType()) && field.getSelect() != null && !field.getSelect().isEmpty()){
                            View subView = getLayoutInflater().inflate(R.layout.shopping_cart_select_field, subContainer, false);
                            RadioGroup group = subView.findViewById(R.id.rg);
                            for(String key : field.getSelect().keySet()){
                                RadioButton button = new RadioButton(this);
                                button.setText(field.getSelect().get(key));
                                button.setTextColor(ContextCompat.getColor(this, R.color.contentText));
                                button.setTag(key);
                                int[][] states = new int[][] {
                                        new int[] {-android.R.attr.state_checked}, // disabled
                                        new int[] { android.R.attr.state_checked}, // unchecked
                                };

                                int[] colors = new int[] {
                                        ContextCompat.getColor(this, R.color.contentText),
                                        ContextCompat.getColor(this, R.color.jOrange)

                                };

                                ColorStateList cl = new ColorStateList(states, colors);
                                button.setButtonTintList(cl);
                                group.addView(button);

                            }
                            subContainer.addView(subView);
                        }else if("gps_current_location".equals(field.getType())){
                            final View subView = getLayoutInflater().inflate(R.layout.gps_button, subContainer, false);
                            final TextView label = subView.findViewById(R.id.text1);

                            subView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    mGPSView = subView;
                                    if(ContextCompat.checkSelfPermission(ShoppingCartActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                                        subView.setAlpha(0.5f);
                                        Intent intent = new Intent(ShoppingCartActivity.this, GPSService.class);
                                        label.setText("Acquisizione in corso...");
                                        startService(intent);
                                    }else{
                                        ActivityCompat.requestPermissions(ShoppingCartActivity.this, new String[]{
                                                Manifest.permission.ACCESS_FINE_LOCATION
                                        }, 222);
                                    }
                                }
                            });
                            subContainer.addView(subView);
                        }
                    }

                    container.addView(subview);
                }

                superview.addView(view);
            }
            superview.addView(paymentView);

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        mGPSView.setAlpha(0.5f);
        Intent intent = new Intent(ShoppingCartActivity.this, GPSService.class);
        startService(intent);
        final TextView label = mGPSView.findViewById(R.id.text1);
        label.setText("Acquisizione in corso...");
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private final class PaymentResultCallback
            implements ApiResultCallback<PaymentIntentResult> {
        @NonNull private final WeakReference<ShoppingCartActivity> activityRef;

        PaymentResultCallback(@NonNull ShoppingCartActivity activity) {
            activityRef = new WeakReference<>(activity);
        }

        @Override
        public void onSuccess(@NonNull PaymentIntentResult result) {
            final ShoppingCartActivity activity = activityRef.get();
            if (activity == null) {
                return;
            }

            PaymentIntent paymentIntent = result.getIntent();
            PaymentIntent.Status status = paymentIntent.getStatus();
            if (status == PaymentIntent.Status.Succeeded) {
                // Payment completed successfully
             //   Gson gson = new GsonBuilder().setPrettyPrinting().create();
            //    Toast.makeText(activity, "Payment completed", Toast.LENGTH_LONG).show();
            /*    activity.displayAlert(
                        "Payment completed",
                        gson.toJson(paymentIntent),
                        true
                );*/
                Map<String, String> map = new HashMap<>();
                String format = "payment[stripe][%s]";
                map.put(String.format(format, "id"), paymentIntent.getId());
                map.put(String.format(format, "amount"), String.valueOf(paymentIntent.getAmount()));
                map.put(String.format(format, "status"), paymentIntent.getStatus().name());
                map.put(String.format(format, "clientSecret"), paymentIntent.getClientSecret());
                map.put(String.format(format, "confirmationMethod"), paymentIntent.getConfirmationMethod());
                submit(map);
            try{
                ObjectMapper mapper = JJApplication.getInstance().getObjectMapper();
                String j = mapper.writeValueAsString(paymentIntent);
                Log.d("payment", j);
            }catch(IOException e){
                Log.e("", "", e);
            }
            } else if (status == PaymentIntent.Status.RequiresPaymentMethod) {
                // Payment failed
                Toast.makeText(activity, "Errore durante il pagamento.", Toast.LENGTH_LONG).show();
           /*     activity.displayAlert(
                        "Payment failed",
                        Objects.requireNonNull(paymentIntent.getLastPaymentError()).getMessage(),
                        false
                );*/
            }
        }

        @Override
        public void onError(@NonNull Exception e) {
            final ShoppingCartActivity activity = activityRef.get();
            if (activity == null) {
                return;
            }

            // Payment request failed – allow retrying using the same payment method
            Toast.makeText(activity, "Error", Toast.LENGTH_LONG).show();
          //  activity.displayAlert("Error", e.toString(), false);
        }
    }



    private class Adapter extends RecyclerView.Adapter<Adapter.ViewHolder>{

        private static final int BOTTOM_VIEW_TYPE = 1, HEADER_VIEW = 2, CHECK_FIELDS_FORM = 3;
        private List<Product> mDataset;
        private CheckoutData mData;
        public Adapter(List<Product> mDataset){
            this.mDataset = mDataset;
            this.mData = mData;
        }

        @Override
        public int getItemViewType(int position) {


                return 0;

        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

                View view = getLayoutInflater().inflate(R.layout.shopping_cart_list_item, parent, false);
                return new ProductViewHolder(view);


        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
                if(position < mDataset.size()){
                    final Product product = mDataset.get(position);
                    final ProductViewHolder viewHolder = (ProductViewHolder) holder;
                    viewHolder.setProduct(product);

                    viewHolder.name.setText(product.getName());

                    final SharedPreferences prefs = getSharedPreferences(MenuActivity.PRODUCT_Q, Context.MODE_PRIVATE);
                    int q = prefs.getInt(getProductQuantityKey(product), 0);
                    viewHolder.quantity.setText(String.valueOf(q));
                    viewHolder.setPrice(q);

                    viewHolder.add.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            int q = prefs.getInt(getProductQuantityKey(product), 0);
                            SharedPreferences.Editor editor = prefs.edit();
                            editor.putInt(getProductQuantityKey(product), ++q);
                            editor.commit();
                            viewHolder.quantity.setText(String.valueOf(q));
                            viewHolder.setPrice(q);
                            updateSum();
                        }
                    });

                    viewHolder.remove.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            int q = prefs.getInt(getProductQuantityKey(product), 0);
                            if(q == 0){
                                return;
                            }
                            SharedPreferences.Editor editor = prefs.edit();
                            editor.putInt(getProductQuantityKey(product), --q);
                            editor.commit();
                            viewHolder.quantity.setText(String.valueOf(q));
                            viewHolder.setPrice(q);
                            updateSum();
                        }
                    });
                }else{

                }
        }

        @Override
        public int getItemCount() {
            return mDataset.size() ;
        }

        private class ViewHolder extends RecyclerView.ViewHolder{
            ViewHolder(View v){
                super(v);
            }
        }

        private class ProductViewHolder extends ViewHolder{

            public final Button add, remove;
            public final TextView quantity, name, price;
            private Product product;

            ProductViewHolder(View v){
                super(v);
                add = v.findViewById(R.id.buttonAdd);
                remove = v.findViewById(R.id.buttonRemove);
                name = v.findViewById(R.id.name);
                quantity = v.findViewById(R.id.quantity);
                price = v.findViewById(R.id.price);
            }

            public void setPrice(int quantity) {
                try {
                    Double price = Double.parseDouble(product.getRegular_price());
                    NumberFormat format = new DecimalFormat("#.##");

                    format.setMinimumFractionDigits(2);
                    this.price.setText("\u20ac" + format.format(quantity * price) );
                } catch (NumberFormatException e) {
                    Log.e("", "error converting price.", e);
                    this.price.setText("");
                }
            }

            public void setProduct(Product product) {
                this.product = product;
            }
        }
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            String message = intent.getStringExtra("Status");
            Bundle b = intent.getBundleExtra("Location");
            Location lastKnownLoc = (Location) b.getParcelable("Location");
            mShippingMethod.setLocation(lastKnownLoc);
            mGPSView.setBackground(ContextCompat.getDrawable(getBaseContext(), R.drawable.product_btn_add));
            mGPSView.setAlpha(1.0f);
            TextView label = mGPSView.findViewById(R.id.text1);
            label.setText("Posizione acquisita: " + lastKnownLoc.getLatitude() + ", " + lastKnownLoc.getLongitude());

            Intent serviceIntent = new Intent(ShoppingCartActivity.this, GPSService.class);
            stopService(serviceIntent);
        }
    };
}
