package eu.contat.jamesjoice.main;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Map;

import eu.contat.jamesjoice.R;
import eu.contat.jamesjoice.model.CartOrderProduct;
import eu.contat.jamesjoice.model.OrderResponse;
import eu.contat.jamesjoice.model.order.OrderBilling;
import eu.contat.jamesjoice.model.order.OrderPayment;
import eu.contat.jamesjoice.model.order.OrderShipping;

public class OrderDetailActivity extends AppCompatActivity {

    public static String DATA_KEY = "orderDetailData";

    private OrderResponse mData;
    private LinearLayout mContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        mContainer = findViewById(R.id.body);

        Intent intent = getIntent();
        if(intent != null){
            mData = intent.getParcelableExtra(DATA_KEY);
            populateLayout();
        }
    }

    private void populateLayout(){
        if(this.mData != null){
            ProgressBar bar = findViewById(R.id.progress);
            bar.setProgress(mData.getProgress());

            if(mData.getCart() != null) {
                for (CartOrderProduct p : mData.getCart()) {
                    View view = getLayoutInflater().inflate(R.layout.order_detail_label, mContainer, false);
                    TextView title = view.findViewById(R.id.title);
                    TextView value = view.findViewById(R.id.value);

                    title.setText(p.getName());
                    value.setText("x" + p.getQty());

                    mContainer.addView(view);

                    if(p.getMetadata() != null && !p.getMetadata().isEmpty()){
                        for(CartOrderProduct.MetaData meta : p.getMetadata()) {
                            if(meta.getKey() != null && meta.getKey().equals("addons")) {
                                String v = meta.getValue();
                                View subView = getLayoutInflater().inflate(R.layout.order_detail_add, mContainer, false);
                                TextView text = subView.findViewById(R.id.text);
                                text.setText("+ " + v);
                                mContainer.addView(subView);
                            }
                        }
                    }
                }
            }

            if(mData.getShipping() != null && mData.getBilling() != null){
                OrderShipping shipping = mData.getShipping();
                OrderBilling billing = mData.getBilling();
                View view = getLayoutInflater().inflate(R.layout.order_detail_card, mContainer, false);

                TextView title = view.findViewById(R.id.title);
                title.setText("Indirizzo di Consegna");
                LinearLayout container = view.findViewById(R.id.body);

                Map<String, String> data = new HashMap<>();
                data.put("Metodo", shipping.getMethod());
                data.put("Indirizzo", billing.getAddress1());
                data.put("Destinatario", billing.getFirstName() + " " + billing.getLastName());

                for(String key: data.keySet()){
                    View labelView = getLayoutInflater().inflate(R.layout.order_detail_label, mContainer, false);
                    TextView name = labelView.findViewById(R.id.title);
                    TextView value = labelView.findViewById(R.id.value);

                    name.setText(key);
                    value.setText(data.get(key));

                    container.addView(labelView);
                }

                mContainer.addView(view);
            }

            if(mData.getPayment() != null){
                OrderPayment payment = mData.getPayment();
                View view = getLayoutInflater().inflate(R.layout.order_detail_card, mContainer, false);

                TextView title = view.findViewById(R.id.title);
                title.setText("Metodo di Pagamento");
                LinearLayout container = view.findViewById(R.id.body);

                View labelView = getLayoutInflater().inflate(R.layout.order_detail_label, mContainer, false);
                TextView name = labelView.findViewById(R.id.title);
                TextView value = labelView.findViewById(R.id.value);

                name.setText("Metodo");
                value.setText(payment.getName());

                container.addView(labelView);

                mContainer.addView(view);
            }


        }
    }
}
