package eu.contat.jamesjoice.main;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.percentlayout.widget.PercentFrameLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.PersistableBundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

import eu.contat.jamesjoice.JJApplication;
import eu.contat.jamesjoice.R;

import eu.contat.jamesjoice.model.Category;
import eu.contat.jamesjoice.model.JSendResponse;
import eu.contat.jamesjoice.model.MenuEntity;
import eu.contat.jamesjoice.model.MenuHeaderData;
import eu.contat.jamesjoice.model.Product;
import eu.contat.jamesjoice.model.ServerImage;
import eu.contat.jamesjoice.worker.ProductWorker;

public class MenuActivity extends AppCompatActivity implements Response.Listener<String>, Response.ErrorListener{

    private static final String PRODUCT_LIST_KEY = "products",
            CATEGORIES_LIST_KEY = "categories",
            ENTITY_LIST_KEY = "entities",
            HEDAER_DATA_KEY = "header",
            SELECTED_TAB_KEY = "tab";

    public static final String PRODUCT_Q = "product_quantity";

    private SortedSet<Category> mCategories = new TreeSet<>();
  //  private TreeMap<Category, List<Product>> mTreeMap = new TreeMap<>();
    private List<Product> mProducts;
    private List<MenuEntity> mEntities;
    private TabLayout mTabLayout;
    private RecyclerView mRecycleView;
    private boolean scrolling = false, selectingFromProduct = false;
    private ImageView backgroundImage;
    private ProgressBar mProgressBar;
    private MenuHeaderData mHeaderData;
    private TextView mCounter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(null);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_menu);


        mTabLayout = (TabLayout) findViewById(R.id.tab_layout);

        mCounter = findViewById(R.id.counter);
        mCounter.setVisibility(View.GONE);

        mRecycleView = (RecyclerView) findViewById(R.id.recycle);

        mProgressBar = findViewById(R.id.progressbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MenuActivity.this, ShoppingCartActivity.class);
                ArrayList<Product> list = new ArrayList<>();
                SharedPreferences prefs = getSharedPreferences(PRODUCT_Q, Context.MODE_PRIVATE);
                for(Product p : mProducts){
                    int q = prefs.getInt(getProductQuantityKey(p), 0);
                    if(q > 0){
                        list.add(p);
                    }
                }
                if(!list.isEmpty()) {
                    intent.putParcelableArrayListExtra(ShoppingCartActivity.PRODUCT_LIST_KEY, list);
                    startActivity(intent);
                }else{
                    Toast.makeText(MenuActivity.this, "Il carrello è vuoto.", Toast.LENGTH_LONG);
                }
            }
        });

       final LinearLayoutManager layoutManager = new LinearLayoutManager(this);

        mRecycleView.setLayoutManager(layoutManager);

        mRecycleView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE) {
                    scrolling = false;
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });

        mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if(mProducts == null)
                    return;
                if(selectingFromProduct){
                    selectingFromProduct = false;
                    return;
                }
                Category c = (Category) tab.getTag();
                int index = mEntities.indexOf(c);
                if(index != -1)
                layoutManager.scrollToPositionWithOffset(index, 0);
        /*        Product p = null;
               f: for(Product prod : mProducts){
                    if(c.equals(prod.getCategory())) {
                        p = prod;
                        break f;
                    }
                }
                if(p != null) {
                    int index = mProducts.indexOf(p);
                    scrolling = true;
                   // mRecycleView.smoothScrollToPosition(index);
                    //layoutManager.findFirstVisibleItemPosition()
                    layoutManager.scrollToPositionWithOffset(index, 0);
                   // layoutManager.scrollToPosition(index);
                }

         */
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        this.backgroundImage = findViewById(R.id.backImage);

        mProgressBar.setVisibility(View.VISIBLE);
        SharedPreferences prefs = getPreferences(Context.MODE_PRIVATE);

        if(savedInstanceState != null && !savedInstanceState.isEmpty() &&
                savedInstanceState.containsKey(HEDAER_DATA_KEY) &&
            prefs.contains(PRODUCT_LIST_KEY) && prefs.contains(CATEGORIES_LIST_KEY)){
          //  mProducts = savedInstanceState.getParcelableArrayList(PRODUCT_LIST_KEY);
          //  List<Category> cats = savedInstanceState.getParcelableArrayList(CATEGORIES_LIST_KEY);
            mCategories.clear();
         //   mCategories.addAll(cats);
            mHeaderData = savedInstanceState.getParcelable(HEDAER_DATA_KEY);
          //  mEntities = savedInstanceState.getParcelableArrayList(ENTITY_LIST_KEY);
        try {
            ObjectMapper mapper = JJApplication.getInstance().getObjectMapper();

            mProducts = mapper.readValue(prefs.getString(PRODUCT_LIST_KEY, ""), new TypeReference<List<Product>>(){});
            Set<Category> cats = mapper.readValue(prefs.getString(CATEGORIES_LIST_KEY, ""), new TypeReference<Set<Category>>(){});
            for(Category c : cats){
                c.setOrder(prefs.getInt(String.valueOf(c.getId()), -1));
            }
            mCategories.addAll(cats);

            for(Product product : mProducts){
                Category category;
                for(Category c : mCategories){
                    if(product.getCategories() != null &&
                            !product.getCategories().isEmpty()){
                        if( product.getCategories().get(0).equals(c.getId())){
                            product.setCategory(c);
                        }
                    }
                }
            }

            Collections.sort(mProducts);
            createEntityList();


             for (Category c : mCategories) {
                mTabLayout.addTab(mTabLayout.newTab().setText(c.getName()).setTag(c));
            }


            MyAdapter adapter = new MyAdapter(mEntities);
            mRecycleView.setAdapter(adapter);
             adapter.notifyDataSetChanged();

            int indexTab = savedInstanceState.getInt(SELECTED_TAB_KEY);
             mTabLayout.getTabAt(indexTab).select();
            }catch(IOException e){
                e.printStackTrace();
            }finally {
            mProgressBar.setVisibility(View.GONE);
        }

            ImageLoader imageLoader = JJApplication.getInstance().getImageLoader();
            MenuHeaderData.Data data = mHeaderData.getHeader().length > 0 ? mHeaderData.getHeader()[0] : null;
            imageLoader.get(data.getSrc(), new ImageLoader.ImageListener() {
                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                    if (response.getBitmap() != null) {
                        backgroundImage.setImageBitmap(response.getBitmap());
                    }
                }

                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });

        }else {

            prefs = getSharedPreferences(PRODUCT_Q, Context.MODE_PRIVATE);
            prefs.edit().clear().commit();

            new ProductWorker().requestCategorie(this, this);
            new ProductWorker().requestHeader(new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    ObjectMapper mapper = JJApplication.getInstance().getObjectMapper();
                    try {
                        MenuHeaderData jsonResponse = mapper.readValue(response, new TypeReference<MenuHeaderData>() {
                        });
                        mHeaderData = jsonResponse;
                        MenuHeaderData.Data data = jsonResponse.getHeader().length > 0 ? jsonResponse.getHeader()[0] : null;

                        ImageLoader imageLoader = JJApplication.getInstance().getImageLoader();
                        imageLoader.get(data.getSrc(), new ImageLoader.ImageListener() {
                            @Override
                            public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                                if (response.getBitmap() != null) {
                                    backgroundImage.setImageBitmap(response.getBitmap());
                                }
                            }

                            @Override
                            public void onErrorResponse(VolleyError error) {

                            }
                        });


                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(mEntities!= null && !mEntities.isEmpty() && mHeaderData != null) {
            outState.putParcelable(HEDAER_DATA_KEY, mHeaderData);

            outState.putInt(SELECTED_TAB_KEY, mTabLayout.getSelectedTabPosition());
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        JJApplication app = JJApplication.getInstance();
        app.cancelPendingRequests(ProductWorker.PRODUCT_LIST_TAG);
        app.cancelPendingRequests(ProductWorker.CATEGORY_LIST_TAG);
        app.cancelPendingRequests(ProductWorker.HEADER_TAG);
        if(mEntities == null || mEntities.isEmpty()){
            return;
        }

        new AsyncTask<Void, Void, Void>(){

            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    ObjectMapper mapper = JJApplication.getInstance().getObjectMapper();
                    String jsonData = mapper.writeValueAsString(mProducts);
                    String jsonDataC = mapper.writeValueAsString(mCategories);
                    SharedPreferences prefs = getPreferences(Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString(PRODUCT_LIST_KEY, jsonData);
                    editor.putString(CATEGORIES_LIST_KEY, jsonDataC);
                    for(Category c : mCategories){
                        editor.putInt(String.valueOf(c.getId()), c.getOrder());
                    }
                    editor.commit();
                }catch(IOException e){
                    e.printStackTrace();
                }
                return null;
            }
        }.execute();
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateCounter();
        if(mRecycleView.getAdapter() != null) {
            mRecycleView.getAdapter().notifyDataSetChanged();
        }
    }

    private void updateCounter(){
        if(mProducts != null) {
            SharedPreferences prefs = getSharedPreferences(PRODUCT_Q, MODE_PRIVATE);
            int sum = 0;
            for (Product p : mProducts) {
                sum += prefs.getInt(getProductQuantityKey(p), 0);
            }

            mCounter.setVisibility(sum > 0 ? View.VISIBLE : View.GONE);
            mCounter.setTag(new Integer(sum));
            mCounter.setText(String.valueOf(sum));
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        try{
            Log.e("", "", error);
        }finally {
            mProgressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void onResponse(String response) {
        ObjectMapper mapper = JJApplication.getInstance().getObjectMapper();

        try{
            JSendResponse<List<Category>> jsonResponse = mapper.readValue(response, new TypeReference<JSendResponse<List<Category>>>(){});
            if(jsonResponse.getStatus() != null && jsonResponse.getStatus().equals(JSendResponse.STATUS_success)){
                if(jsonResponse.getData() != null) {
                    List<Category> list = jsonResponse.getData();
                    for(Category c : list){
                        c.setOrder(list.indexOf(c));
                    }
                    this.mCategories.clear();
                    this.mCategories.addAll(jsonResponse.getData());


                    mProgressBar.setVisibility(View.VISIBLE);
                    requestProducts();
                }
            }
        }catch(IOException e){
            e.printStackTrace();
        }
    }

    private void requestProducts(){
        new ProductWorker().requestProducts(new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                ObjectMapper mapper = JJApplication.getInstance().getObjectMapper();

                try{
                    JSendResponse<List<Product>> jsonResponse = mapper.readValue(response, new TypeReference<JSendResponse<List<Product>>>(){});
                    if(jsonResponse.getStatus() != null && jsonResponse.getStatus().equals(JSendResponse.STATUS_success)){
                        if(jsonResponse.getData() != null) {
                            List<Product> products = jsonResponse.getData();


                            Set<Product> orphans = new HashSet<>();
                            Set<Category> orphansCategories = new HashSet<>();
                            for(Product product : products){
                                Category category = null;
                             fr:   for(Category c : mCategories){
                                    if(product.getCategories() != null &&
                                            !product.getCategories().isEmpty()){
                                           if( product.getCategories().get(0).equals(c.getId())){
                                            product.setCategory(c);
                                            category = c;
                                            break fr;
                                        }
                                    }else{
                                        orphans.add(product);
                                    }
                                }
                                if(category == null){
                                    orphans.add(product);
                                }


                            }

                            products.removeAll(orphans);

                            for(Category c : mCategories){
                                b:{
                                    for(Product product : products){
                                        if( product.getCategories() != null &&
                                                !product.getCategories().isEmpty() &&
                                                product.getCategories().get(0).equals(c.getId())){
                                            break b;
                                        }

                                    }
                                    orphansCategories.add(c);
                                }

                            }
                            mCategories.removeAll(orphansCategories);

                            for(Category c : mCategories){
                                mTabLayout.addTab(mTabLayout.newTab().setText(c.getName() ).setTag(c));
                                //   mTreeMap.put(c, new ArrayList<Product>());
                            }

                            mProducts = products;
                            Collections.sort(mProducts);

                          createEntityList();



                            MyAdapter adapter = new MyAdapter(mEntities);
                            mRecycleView.setAdapter(adapter);
                            adapter.notifyDataSetChanged();
                        }
                    }
                }catch(IOException e){
                    e.printStackTrace();
                }finally {
                    mProgressBar.setVisibility(View.GONE);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try{

                }finally {
                    mProgressBar.setVisibility(View.GONE);
                }
            }
        });
    }

    private void createEntityList(){
        List<Product> products = mProducts;
        List<MenuEntity> entities = new ArrayList<>();

        for(int i=0; i < products.size(); i++){
            Product p = products.get(i);
            if(i == 0){
                entities.add(p.getCategory());
            }else {
                Product previous = products.get(i - 1);
                if(previous.getCategory() != null &&
                        !previous.getCategory().equals(p.getCategory()) &&
                        p.getCategory() != null
                ){
                    entities.add(p.getCategory());
                }
            }
            entities.add(p);
        }
        mEntities = entities;
    }

    public static String getProductQuantityKey(Product p){
        return PRODUCT_Q + String.valueOf(p.getId());
    }

    public interface DynamicHeight {
        void heightChange (int position, int height);
    }

    public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder>  {

        private static final int CATEGORY = 1, PRODUCT = 2, FOOTER = 3;

        private List<MenuEntity> mDataset;



        // Provide a reference to the views for each data item
        // Complex data items may need more than one view per item, and
        // you provide access to all the views for a data item in a view holder
        public  class MyViewHolder extends RecyclerView.ViewHolder {


            public MyViewHolder(View v) {
                super(v);

            }
        }

        public class CategoryViewHolder extends MyViewHolder{

            private Category category;
            public final TextView textView;
            public CategoryViewHolder(View v) {
                super(v);
                textView = v.findViewById(R.id.name);
            }

            public Category getCategory() {
                return category;

            }

            public void setCategory(Category category) {
                this.category = category;
            }
        }

        public class ProductViewHolder extends MyViewHolder{
            // each data item is just a string in this case
            public final TextView description, price, quantity, textView;
            public final ImageView imageView;
            private Product product;
            public final View imageFrame, view;
            public final Button add, remove;
            public ProductViewHolder(View v) {
                super(v);
                view = v;
                textView = v.findViewById(R.id.name);
                description = v.findViewById(R.id.description);
                imageView = v.findViewById(R.id.productImage);
                imageFrame = v.findViewById(R.id.imageFrame);
                imageView.setClipToOutline(true);
                price = v.findViewById(R.id.price);
                quantity = v.findViewById(R.id.quantity);
                add = v.findViewById(R.id.buttonAdd);
                remove = v.findViewById(R.id.buttonRemove);
            }

            public Product getProduct() {
                return product;
            }

            public void setProduct(Product product) {
                this.product = product;
            }
        }

        // Provide a suitable constructor (depends on the kind of dataset)
        public MyAdapter(List<MenuEntity> myDataset) {
            mDataset = myDataset;
        }





        // Create new views (invoked by the layout manager)
        @Override
        public MyAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                         int viewType) {
            // create a new view
            if(viewType == PRODUCT) {
                View v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.product_list_item, parent, false);

                MyViewHolder vh = new ProductViewHolder(v);
                return vh;
            }else if(viewType == CATEGORY){
                View v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.category_list_item, parent, false);
                MyViewHolder vh = new CategoryViewHolder(v);
                return vh;
            }else{
                View v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.product_list_footer, parent, false);
                MyViewHolder vh = new MyViewHolder(v);
                return vh;
            }
        }

        // Replace the contents of a view (invoked by the layout manager)
        @Override
        public void onBindViewHolder(final MyViewHolder h, final int position) {
            // - get element from your dataset at this position
            // - replace the contents of the view with that element
            MenuEntity item = position < mDataset.size() ? mDataset.get(position) : null;

            if(item instanceof  Product && h instanceof ProductViewHolder) {
                final ProductViewHolder holder = (ProductViewHolder) h;
                final Product product = (Product) item;
                holder.setProduct(product);
                holder.textView.setText(product.getName());
                holder.description.setVisibility(product.getContent() != null && !product.getContent().trim().isEmpty() ? View.VISIBLE : View.GONE);
                holder.description.setText(product.getContent());
                holder.price.setText(product.getRegular_price() + " \u20ac");
                holder.imageView.setImageDrawable(null);
                if (!product.getImage().isEmpty()) {
                    holder.imageFrame.setVisibility(View.VISIBLE);
                    ImageLoader loader = JJApplication.getInstance().getImageLoader();
                    ServerImage.Image img = product.getImage().get(0).getFull();

                    ConstraintLayout.LayoutParams lp = (ConstraintLayout.LayoutParams) holder.imageView.getLayoutParams();
                    lp.matchConstraintPercentHeight = (float) img.getWidth() / img.getHeight();

                    // layoutParams.getPercentLayoutInfo().aspectRatio = img.getWidth() / img.getHeight();
                    holder.imageView.setLayoutParams(lp);

                    loader.get(img.getUrl(), new ImageLoader.ImageListener() {
                        @Override
                        public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                            if (response.getBitmap() != null) {
                                //some code
                                holder.imageFrame.setVisibility(View.VISIBLE);
                                holder.imageView.setImageBitmap(response.getBitmap());
                            } else {
                                holder.imageFrame.setVisibility(View.GONE);
                            }
                        }

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            holder.imageFrame.setVisibility(View.GONE);
                        }
                    });
                    //   holder.imageView.setImageUrl(product.getImage().get(0).getFull(), );

                } else {
                    holder.imageFrame.setVisibility(View.GONE);
                }

                final SharedPreferences prefs = getSharedPreferences(PRODUCT_Q, Context.MODE_PRIVATE);
                int q = prefs.getInt(getProductQuantityKey(product), 0);
                holder.quantity.setText(String.valueOf(q));

                holder.add.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int q = prefs.getInt(getProductQuantityKey(product), 0);
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putInt(getProductQuantityKey(product), ++q);
                        editor.commit();
                        holder.quantity.setText(String.valueOf(q));
                        updateCounter();
                    }
                });

                holder.remove.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int q = prefs.getInt(getProductQuantityKey(product), 0);
                        if(q == 0){
                            return;
                        }
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putInt(getProductQuantityKey(product), --q);
                        editor.commit();
                        holder.quantity.setText(String.valueOf(q));
                        updateCounter();
                    }
                });
            }else if(item instanceof Category && h instanceof CategoryViewHolder){
                Category category = (Category) item;
                CategoryViewHolder holder = (CategoryViewHolder) h;
                holder.setCategory(category);
                holder.textView.setText(category.getName());
            }

        }

        @Override
        public int getItemViewType(int position) {
            MenuEntity item = position < mDataset.size() ? mDataset.get(position) : null;
            if(item instanceof Product){
                return PRODUCT;
            }else if(item instanceof Category){
                return CATEGORY;
            }else{
                return FOOTER;
            }
        }

        // Return the size of your dataset (invoked by the layout manager)
        @Override
        public int getItemCount() {
            return mDataset.size() + 1;
        }

        @Override
        public void onViewAttachedToWindow(@NonNull MyAdapter.MyViewHolder h) {
            super.onViewAttachedToWindow(h);
            if(scrolling){
                return;
            }
            if(h instanceof ProductViewHolder) {
                ProductViewHolder holder = (ProductViewHolder)h;
                Product product = holder.getProduct();
                if (product.getCategory() != null && product.getCategory().getOrder() >= 0) {
                    //     if(holder instanceof CategoryViewHolder){
                    Category category = product.getCategory();
                    int index = mTabLayout.getSelectedTabPosition();
                    Category selected = (Category) mTabLayout.getTabAt(index).getTag();
                    if (!category.equals(selected)) {
                        selectingFromProduct = true;
                        mTabLayout.getTabAt(category.getOrder()).select();
                    }
                }
            }

        }
    }
}
