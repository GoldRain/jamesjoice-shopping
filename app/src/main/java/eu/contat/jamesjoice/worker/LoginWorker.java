package eu.contat.jamesjoice.worker;

import android.net.Uri;


import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import eu.contat.jamesjoice.BuildConfig;
import eu.contat.jamesjoice.JJApplication;
import eu.contat.jamesjoice.server.CustomPostRequest;


public class LoginWorker {

    public static final String LOGINT_TAG = "login";
    public static final String REGISTER_TAG = "login";

    public void login(Response .Listener<String> listener, Response.ErrorListener errorListener, String username, String password){
        String url = new Uri.Builder()
                .encodedPath(BuildConfig.SERVER_URL)
                .appendPath("v1")
                .appendPath("users")
                .appendPath("auth")
                .build()
                .toString();

        Map<String, String> params = new HashMap<>();
        params.put("email", username);
        params.put("pass", password);
        String device = android.os.Build.MODEL;
        params.put("device", device);

        StringRequest req = new CustomPostRequest(Request.Method.POST, url, listener, errorListener, params);
        JJApplication.getInstance().addToRequestQueue(req, LOGINT_TAG);
    }

    public void register(
            Response.Listener<String> listener,
            Response.ErrorListener errorListener,
            String username,
            String password,
            String confirmPassword,
            String phone,
            String name,
            String surname){
        String url = new Uri.Builder()
                .encodedPath(BuildConfig.SERVER_URL)
                .appendPath("v1")
                .appendPath("users")
                .build()
                .toString();

        Map<String, String> params = new HashMap<>();
        params.put("email", username);
        params.put("pass", password);
        params.put("pass_confirm", confirmPassword);
        params.put("first_name", name);
        params.put("last_name", surname);
        params.put("meta[phone]", phone);
        params.put("meta[test]", "test");

        StringRequest req = new CustomPostRequest(Request.Method.POST, url, listener, errorListener, params);
        JJApplication.getInstance().addToRequestQueue(req, REGISTER_TAG);
    }
}
