package eu.contat.jamesjoice.worker;

import android.net.Uri;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import eu.contat.jamesjoice.BuildConfig;
import eu.contat.jamesjoice.JJApplication;

public class ProductWorker {

    public static final String PRODUCT_LIST_TAG = "products";
    public static final String CATEGORY_LIST_TAG = "categories";
    public static final String HEADER_TAG = "header";

    public void requestProducts(Response.Listener<String> listener, Response.ErrorListener errorListener) {
        String url = new Uri.Builder()
                .encodedPath(BuildConfig.SERVER_URL)
                .appendPath("v1")
                .appendPath("products")
                .build()
                .toString();

        StringRequest request;
        request = new StringRequest(Request.Method.GET, url, listener, errorListener);
        JJApplication.getInstance().addToRequestQueue(request, PRODUCT_LIST_TAG);
    }

    public void requestCategorie(Response.Listener<String> listener, Response.ErrorListener errorListener) {
        String url = new Uri.Builder()
                .encodedPath(BuildConfig.SERVER_URL)
                .appendPath("v1")
                .appendPath("categories")
                .build()
                .toString();

        StringRequest request;
        request = new StringRequest(Request.Method.GET, url, listener, errorListener);
        JJApplication.getInstance().addToRequestQueue(request, CATEGORY_LIST_TAG);
    }

    public void requestHeader(Response.Listener<String> listener, Response.ErrorListener errorListener) {
        String url = new Uri.Builder()
                .encodedPath(BuildConfig.SERVER_URL)
                .appendPath("v1")
                .appendPath("variables")
                .build()
                .toString();

        StringRequest request;
        request = new StringRequest(Request.Method.GET, url, listener, errorListener);
        JJApplication.getInstance().addToRequestQueue(request, HEADER_TAG);
    }
}
