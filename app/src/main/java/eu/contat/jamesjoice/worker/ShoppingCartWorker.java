package eu.contat.jamesjoice.worker;

import android.net.Uri;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import eu.contat.jamesjoice.BuildConfig;
import eu.contat.jamesjoice.JJApplication;
import eu.contat.jamesjoice.model.PaymentMethod;
import eu.contat.jamesjoice.model.ShippingMethod;
import eu.contat.jamesjoice.server.CustomPostRequest;

public class ShoppingCartWorker {

    public void checkout(Response.Listener<String> listener, Response.ErrorListener errorListener, LinkedHashMap<Long, Integer> orders) {
        Uri.Builder builder = new Uri.Builder()
                .encodedPath(BuildConfig.SERVER_URL)
                .appendPath("v1")
                .appendPath("checkout");
        List<Long> list = new ArrayList<>(orders.keySet());
        for(int i = 0; i< orders.size(); i++){
            Long key = list.get(i);
            builder = builder.appendQueryParameter("cart["+i+"][id]", String.valueOf(key));
            builder = builder.appendQueryParameter("cart["+i+"][qty]", String.valueOf(orders.get(key)));
        }
        String url = builder.build().toString();


        StringRequest request;
        request = new StringRequest(Request.Method.GET, url, listener, errorListener);
        JJApplication.getInstance().addToRequestQueue(request, "check");
    }

    public void submitOrder(
            Response.Listener<String> listener,
            Response.ErrorListener errorListener,
            LinkedHashMap<Long, Integer> orders,
            ShippingMethod shippingMethod,
            PaymentMethod paymentMethod,
            Map<String, List<String>> fieldParams,
            Long id,
            Map<String, String> additionalParams
    ){
        Uri.Builder builder = new Uri.Builder()
                .encodedPath(BuildConfig.SERVER_URL)
                .appendPath("v1")
                .appendPath("checkout");
        String url = builder.build().toString();

        Map<String, String> params = new HashMap<>();
        List<Long> list = new ArrayList<>(orders.keySet());
        for(int i = 0; i< orders.size(); i++){
            Long key = list.get(i);
            params.put("cart["+i+"][id]", String.valueOf(key));
            params.put("cart["+i+"][qty]", String.valueOf(orders.get(key)));
        }
        params.put("shipping[id]", shippingMethod.getId());
        params.put("payment[id]", paymentMethod.getId());

        params.put("order[users_id]", String.valueOf(id));

        for(Map.Entry<String, List<String>> entry : fieldParams.entrySet()) {
            for(String value :  entry.getValue())
            params.put(entry.getKey(), value);
        }

        for(Map.Entry<String, String> entry : additionalParams.entrySet()) {

                params.put(entry.getKey(), entry.getValue());
        }

        if(shippingMethod.getLocation() != null){
            params.put("shipping[gps_location]", shippingMethod.getLocation().getLatitude() + "," + shippingMethod.getLocation().getLongitude());
        }


        StringRequest req = new CustomPostRequest(Request.Method.POST, url, listener, errorListener, params);
        JJApplication.getInstance().addToRequestQueue(req, "order");
    }
}
