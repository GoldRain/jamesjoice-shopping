package eu.contat.jamesjoice.server;

import androidx.annotation.Nullable;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class CustomPostRequest extends StringRequest {

    private Map<String, String> params;

    public CustomPostRequest(
            int method,
            String url,
            Response.Listener<String> listener,
            @Nullable Response.ErrorListener errorListener,
            Map<String, String> params
    ) {
        super(method, url, listener, errorListener);
        this.params = params;
    }


    @Override
    public String getBodyContentType() {
        return "application/x-www-form-urlencoded";//; charset=UTF-8";
    }

    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        Map<String, String> params = new HashMap<String, String>();
        params.putAll(this.params);
        return params;
    }
}
