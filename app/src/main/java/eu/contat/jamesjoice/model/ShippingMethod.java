package eu.contat.jamesjoice.model;

import android.location.Location;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class ShippingMethod {

    private String id, name, descrizione;
    private int price;
    private List<CheckoutField> fields;
    @JsonProperty("payment_methods")
    private List<String> paymentMethods;
    private List<List<String>> conditions;
    @JsonIgnore
    private Location location;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public List<CheckoutField> getFields() {
        return fields;
    }

    public void setFields(List<CheckoutField> fields) {
        this.fields = fields;
    }

    public List<String> getPaymentMethods() {
        return paymentMethods;
    }

    public void setPaymentMethods(List<String> paymentMethods) {
        this.paymentMethods = paymentMethods;
    }

    public List<List<String>> getConditions() {
        return conditions;
    }

    public void setConditions(List<List<String>> conditions) {
        this.conditions = conditions;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}

/*

  {
                "id": "pay-after-confirm",
                "name": "Pagamento dopo conferma",
                "descrizione": "Inviaci il tuo ordine. Verificheremo che tutto sia pronto per l'acquisto e ti invieremo la conferma per procedere al pagamento.",
                "price": 0,
                "fields": [
                    {
                        "name": "gps_location",
                        "type": "text",
                        "label": "Posizione GPS"
                    }
                ],
                "payment_methods": [
                    "free"
                ],
                "conditions": []
            }

 */