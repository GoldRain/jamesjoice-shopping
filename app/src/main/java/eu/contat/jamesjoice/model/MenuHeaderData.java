package eu.contat.jamesjoice.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MenuHeaderData implements Parcelable {

    @JsonProperty("menu_header")
    private Data[] header;

    public MenuHeaderData() {
        super();
    }

    protected MenuHeaderData(Parcel in) {
        header = in.createTypedArray(Data.CREATOR);
    }

    public static final Creator<MenuHeaderData> CREATOR = new Creator<MenuHeaderData>() {
        @Override
        public MenuHeaderData createFromParcel(Parcel in) {
            return new MenuHeaderData(in);
        }

        @Override
        public MenuHeaderData[] newArray(int size) {
            return new MenuHeaderData[size];
        }
    };

    public Data[] getHeader() {
        return header;
    }

    public void setHeader(Data[] header) {
        this.header = header;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedArray(header, i);
    }

    public static class Data implements Parcelable {
        private String action, url, src;
        private int[] size;

        public Data() {
            super();
        }

        protected Data(Parcel in) {
            action = in.readString();
            url = in.readString();
            src = in.readString();
            size = in.createIntArray();
        }

        public static final Creator<Data> CREATOR = new Creator<Data>() {
            @Override
            public Data createFromParcel(Parcel in) {
                return new Data(in);
            }

            @Override
            public Data[] newArray(int size) {
                return new Data[size];
            }
        };

        public String getAction() {
            return action;
        }

        public void setAction(String action) {
            this.action = action;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getSrc() {
            return src;
        }

        public void setSrc(String src) {
            this.src = src;
        }

        public int[] getSize() {
            return size;
        }

        public void setSize(int[] size) {
            this.size = size;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(action);
            parcel.writeString(url);
            parcel.writeString(src);
            parcel.writeIntArray(size);
        }
    }
}

/*
{
    "menu_header": [
        {
            "action": "openurl",
            "url": "http://example.com",
            "src": "https://jamesjoyce.socialtab.it/wp-content/uploads/2020/03/header-image.png",
            "size": [
                1125,
                680
            ]
        }
    ]
}
 */