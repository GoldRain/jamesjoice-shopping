package eu.contat.jamesjoice.model.order;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OrderTotal implements Parcelable {

    private String total, discount;
    @JsonProperty("full_total")
    private String fullTotal;
    @JsonProperty("fees_total")
    private String feesTotal;

    public OrderTotal(){
        super();
    }

    protected OrderTotal(Parcel in) {
        total = in.readString();
        discount = in.readString();
        fullTotal = in.readString();
        feesTotal = in.readString();
    }

    public static final Creator<OrderTotal> CREATOR = new Creator<OrderTotal>() {
        @Override
        public OrderTotal createFromParcel(Parcel in) {
            return new OrderTotal(in);
        }

        @Override
        public OrderTotal[] newArray(int size) {
            return new OrderTotal[size];
        }
    };

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getFullTotal() {
        return fullTotal;
    }

    public void setFullTotal(String fullTotal) {
        this.fullTotal = fullTotal;
    }

    public String getFeesTotal() {
        return feesTotal;
    }

    public void setFeesTotal(String feesTotal) {
        this.feesTotal = feesTotal;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(total);
        parcel.writeString(discount);
        parcel.writeString(fullTotal);
        parcel.writeString(feesTotal);
    }
}
/*
{
            "total": "28.50",
            "full_total": "34.00",
            "fees_total": "2.00",
            "discount": "3.50"
        }
 */