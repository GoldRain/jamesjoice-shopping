package eu.contat.jamesjoice.model.order;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OrderBilling implements Parcelable {

    @JsonProperty("first_name")
    private String firstName;
    @JsonProperty("last_name")
    private String lastName;
    private String company;
    @JsonProperty("address_1")
    private String address1;
    @JsonProperty("address_2")
    private String address2;
    private String city, state, postcode, country, phone;

    public OrderBilling(){
        super();
    }

    protected OrderBilling(Parcel in) {
        firstName = in.readString();
        lastName = in.readString();
        company = in.readString();
        address1 = in.readString();
        address2 = in.readString();
        city = in.readString();
        state = in.readString();
        postcode = in.readString();
        country = in.readString();
        phone = in.readString();
    }

    public static final Creator<OrderBilling> CREATOR = new Creator<OrderBilling>() {
        @Override
        public OrderBilling createFromParcel(Parcel in) {
            return new OrderBilling(in);
        }

        @Override
        public OrderBilling[] newArray(int size) {
            return new OrderBilling[size];
        }
    };

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(firstName);
        parcel.writeString(lastName);
        parcel.writeString(company);
        parcel.writeString(address1);
        parcel.writeString(address2);
        parcel.writeString(city);
        parcel.writeString(state);
        parcel.writeString(postcode);
        parcel.writeString(country);
        parcel.writeString(phone);
    }
}
/*
{
            "first_name": "Mario",
            "last_name": "Rossi",
            "company": "",
            "address_1": "Via Roma 33",
            "address_2": "Palazzo 3° piano",
            "city": "Avezzano",
            "state": "Teramo",
            "postcode": "65024",
            "country": "IT",
            "email": "mario.rossi@gmail.com",
            "phone": "3293340083"
        }
 */