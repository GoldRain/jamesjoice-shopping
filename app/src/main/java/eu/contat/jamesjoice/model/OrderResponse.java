package eu.contat.jamesjoice.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

import eu.contat.jamesjoice.model.order.OrderBilling;
import eu.contat.jamesjoice.model.order.OrderFee;
import eu.contat.jamesjoice.model.order.OrderPayment;
import eu.contat.jamesjoice.model.order.OrderShipping;
import eu.contat.jamesjoice.model.order.OrderTotal;

public class OrderResponse implements Parcelable {

    private Integer progress;
    private List<CartOrderProduct> cart;
    private List<OrderFee> fees;
    private OrderTotal totals;
    private OrderBilling billing;
    private OrderShipping shipping;
    private OrderPayment payment;

    public OrderResponse(){
        super();
    }

    protected OrderResponse(Parcel in) {
        cart = in.createTypedArrayList(CartOrderProduct.CREATOR);
        fees = in.createTypedArrayList(OrderFee.CREATOR);
        totals = in.readParcelable(OrderTotal.class.getClassLoader());
        billing = in.readParcelable(OrderBilling.class.getClassLoader());
        shipping = in.readParcelable(OrderShipping.class.getClassLoader());
        payment = in.readParcelable(OrderPayment.class.getClassLoader());
        progress = in.readInt();
    }

    public static final Creator<OrderResponse> CREATOR = new Creator<OrderResponse>() {
        @Override
        public OrderResponse createFromParcel(Parcel in) {
            return new OrderResponse(in);
        }

        @Override
        public OrderResponse[] newArray(int size) {
            return new OrderResponse[size];
        }
    };

    public List<CartOrderProduct> getCart() {
        return cart;
    }

    public void setCart(List<CartOrderProduct> cart) {
        this.cart = cart;
    }

    public List<OrderFee> getFees() {
        return fees;
    }

    public void setFees(List<OrderFee> fees) {
        this.fees = fees;
    }

    public OrderTotal getTotals() {
        return totals;
    }

    public void setTotals(OrderTotal totals) {
        this.totals = totals;
    }

    public OrderBilling getBilling() {
        return billing;
    }

    public void setBilling(OrderBilling billing) {
        this.billing = billing;
    }

    public OrderShipping getShipping() {
        return shipping;
    }

    public void setShipping(OrderShipping shipping) {
        this.shipping = shipping;
    }

    public OrderPayment getPayment() {
        return payment;
    }

    public void setPayment(OrderPayment payment) {
        this.payment = payment;
    }

    public Integer getProgress() {
        return progress;
    }

    public void setProgress(Integer progress) {
        this.progress = progress;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedList(cart);
        parcel.writeTypedList(fees);
        parcel.writeParcelable(totals, i);
        parcel.writeParcelable(billing, i);
        parcel.writeParcelable(shipping, i);
        parcel.writeParcelable(payment, i);
        parcel.writeInt(progress != null ? progress : 0);
    }
}
/*

{
    "data": {
        "message": "Dati di esempio provvisori",
        "cart": [
            {
                "id": "33",
                "name": "Primo prodotto",
                "qty": "1",
                "total": "90.00",
                "subtotal": "8.50",
                "subtotal_tax": "19.80",
                "total_tax": "19.80",
                "meta_data": [
                    {
                        "key": "Aggiunte selezionate",
                        "value": "Salame, formaggio"
                    }
                ]
            }
        ],
        "fees": [
            {
                "name": "Costo aggiuntivo",
                "total": "0.50"
            },
            {
                "name": "Confezione regalo",
                "total": "2.00"
            }
        ],
        "coupons": [],
        "totals": {
            "total": "28.50",
            "full_total": "34.00",
            "fees_total": "2.00",
            "discount": "3.50"
        },
        "customer_note": "",
        "billing": {
            "first_name": "Mario",
            "last_name": "Rossi",
            "company": "",
            "address_1": "Via Roma 33",
            "address_2": "Palazzo 3° piano",
            "city": "Avezzano",
            "state": "Teramo",
            "postcode": "65024",
            "country": "IT",
            "email": "mario.rossi@gmail.com",
            "phone": "3293340083"
        },
        "shipping": {
            "method": "delivery",
            "latitude": "42.042681",
            "longitude": "13.4327513",
            "accuracy": "30",
            "address": "Via dei Tulipani",
            "civic": "3C",
            "city": "Avezzano"
        },
        "payment": {
            "id": "1",
            "name": "Pagamento alla consegna",
            "price": ""
        }
    },
    "status": "success"
}
 */