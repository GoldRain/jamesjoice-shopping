package eu.contat.jamesjoice.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.Nullable;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class Product implements Comparable<Product>, MenuEntity, Parcelable {

    private Long id;
    private String name;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date created, modified;
    private String permalink, content, regular_price, sale_price;
    private String visibility;
    private String qty;
    private String importdata;
    @JsonDeserialize(using = CustomDeserializer.class)
    private List<ServerImage> image;
    @JsonDeserialize(using = CustomCategoryDeserializer.class)
    private List<Long> categories;
    @JsonIgnore
    private Category category;

    public Product() {
        super();
    }

    protected Product(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readLong();
        }
        name = in.readString();
        created = (java.util.Date) in.readSerializable();
        modified = (java.util.Date) in.readSerializable();
        permalink = in.readString();
        content = in.readString();
        regular_price = in.readString();
        sale_price = in.readString();
        visibility = in.readString();
        qty = in.readString();
        importdata = in.readString();
        image = new ArrayList<>();
        in.readTypedList(image, ServerImage.CREATOR);
        categories = new ArrayList<>();
        in.readList(categories, Integer.class.getClassLoader());
        category = in.readParcelable(Category.class.getClassLoader());
    }

    public static final Creator<Product> CREATOR = new Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel in) {
            return new Product(in);
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    public String getPermalink() {
        return permalink;
    }

    public void setPermalink(String permaLink) {
        this.permalink = permaLink;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getRegular_price() {
        return regular_price;
    }

    public void setRegular_price(String regular_price) {
        this.regular_price = regular_price;
    }

    public String getSale_price() {
        return sale_price;
    }

    public void setSale_price(String sale_price) {
        this.sale_price = sale_price;
    }

    public String getVisibility() {
        return visibility;
    }

    public void setVisibility(String visibility) {
        this.visibility = visibility;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getImportdata() {
        return importdata;
    }

    public void setImportdata(String importdata) {
        this.importdata = importdata;
    }

    public List<ServerImage> getImage() {
        return image;
    }

    public void setImage(List<ServerImage> image) {
        this.image = image;
    }

    public List<Long> getCategories() {
        return categories;
    }

    public void setCategories(List<Long> categories) {
        this.categories = categories;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj != null && getClass().equals(obj.getClass())) {
            Product other = (Product) obj;
            return this.id != null && getId().equals(other.getId());
        } else {
            return false;
        }
    }

    @Override
    public int compareTo(Product product) {
        if (product != null) {
            if (product.getCategory() != null && this.getCategory() != null) {
                int cat = getCategory().compareTo(product.getCategory());
                if (cat == 0) {
                    if (this.getName() != null && product.getName() != null) {
                        return getName().compareTo(product.getName());
                    } else {
                        return this.getName() != null ? -1 : product.getName() != null ? 1 : 0;
                    }
                } else {
                    return cat;
                }
            } else {
                return this.getCategory() != null ? -1 : product.getCategory() != null ? 1 : 0;
            }
        }
        return -1;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        if (id == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeLong(id);
        }
        parcel.writeString(name);
        parcel.writeSerializable(created);
        parcel.writeSerializable(modified);
        parcel.writeString(permalink);
        parcel.writeString(content);
        parcel.writeString(regular_price);
        parcel.writeString(sale_price);
        parcel.writeString(visibility);
        parcel.writeString(qty);
        parcel.writeString(importdata);
        parcel.writeTypedList(image);
        parcel.writeList(categories);
        parcel.writeParcelable(category, i);
    }
}

/*



 "id": "269",
            "name": "A Modo Mio IPA  ",
            "created": "0000-00-00 00:00:00",
            "modified": "0000-00-00 00:00:00",
            "permalink": "a-modo-mio-ipa ",
            "content": " ",
            "regular_price": "6.00",
            "sale_price": "0.00",
            "visibility": "show",
            "qty": "0",
            "importdata": "269-|-2015-11-15 21:04:54-|-A Modo Mio IPA -|--|-ITALIA 6,8% CL. 0,33-|--|-6.00-|-http://www.jamesjoyceristopub.it/admin/uploads/items/269_img_6317.jpg-|-45-|-1-|-2-|-1-|-41.60-|-22.00-|-15.00-|-AMMI33-|-0-|-0-|-24-|-269-|-2015-11-15 21:04:54-|-A Modo Mio IPA -|--|-ITALIA 6,8% CL. 0,33-|--|-6.00-|-http://www.jamesjoyceristopub.it/admin/uploads/items/269_img_6317.jpg-|-45-|-1-|-2-|-1-|-41.60-|-22.00-|-15.00-|-AMMI33-|-0-|-0-|-24",
            "image": [
                {
                    "thumb": "https://jamesjoyce.socialtab.it/wp-content/uploads/2020/03/269_img_6317-150x150.jpg",
                    "medium": "https://jamesjoyce.socialtab.it/wp-content/uploads/2020/03/269_img_6317-300x200.jpg",
                    "full": "https://jamesjoyce.socialtab.it/269_img_6317-jpg/"
                }
            ],
            "categories": [
                "45"
            ]
 */