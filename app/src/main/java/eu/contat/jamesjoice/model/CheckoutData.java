package eu.contat.jamesjoice.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class CheckoutData {

    @JsonProperty("checkout_fields")
    private List<List<CheckoutField>> fields;
    @JsonProperty("shipping_methods")
    private List<ShippingMethod> shippingMethods;
    @JsonProperty("payment_methods")
    private List<PaymentMethod> paymentMethods;

    public List<List<CheckoutField>> getFields() {
        return fields;
    }

    public void setFields(List<List<CheckoutField>> fields) {
        this.fields = fields;
    }

    public List<ShippingMethod> getShippingMethods() {
        return shippingMethods;
    }

    public void setShippingMethods(List<ShippingMethod> shippingMethods) {
        this.shippingMethods = shippingMethods;
    }

    public List<PaymentMethod> getPaymentMethods() {
        return paymentMethods;
    }

    public void setPaymentMethods(List<PaymentMethod> paymentMethods) {
        this.paymentMethods = paymentMethods;
    }
}
/*
{
    "data": {
        "checkout_fields": [
            [
                {
                    "name": "first_name",
                    "type": "text",
                    "label": "Nome",
                    "value": "user.meta.first_name"
                },
                {
                    "name": "last_name",
                    "type": "text",
                    "label": "Cognome",
                    "value": "user.meta.last_name"
                },
                {
                    "name": "phone",
                    "type": "text",
                    "label": "Telefono",
                    "value": "user.meta.phone"
                },
                {
                    "name": "email",
                    "type": "text",
                    "label": "Email",
                    "value": "user.user_email"
                },
                {
                    "name": "notes",
                    "type": "text",
                    "label": "Note aggiuntive"
                }
            ]
        ],
        "shipping_methods": [
            {
                "id": "pay-after-confirm",
                "name": "Pagamento dopo conferma",
                "descrizione": "Inviaci il tuo ordine. Verificheremo che tutto sia pronto per l'acquisto e ti invieremo la conferma per procedere al pagamento.",
                "price": 0,
                "fields": [
                    {
                        "name": "gps_location",
                        "type": "text",
                        "label": "Posizione GPS"
                    }
                ],
                "payment_methods": [
                    "free"
                ],
                "conditions": []
            },
            {
                "id": "delivery-paid",
                "name": "Consegna a domicilio",
                "description": "Seleziona questa opzione per ricevere il tuo ordine da un nostro fattorino, la tua spesa è inferiore a €20 e per questo dobbiamo addebidarti un costo di consegna pari a 5€",
                "price": "5",
                "fields": [
                    {
                        "name": "gps_location",
                        "label": "Posizione GPS"
                    }
                ],
                "payment_methods": [
                    "paypal",
                    "stripe"
                ],
                "conditions": [
                    [
                        "total",
                        "<",
                        "20"
                    ],
                    [
                        "distance",
                        "<",
                        "5000"
                    ]
                ]
            },
            {
                "id": "delivery-free",
                "name": "Consegna a domicilio",
                "description": "Ricevi gratuitamente il tuo ordine a casa da un nostro fattorino.",
                "price": "0",
                "fields": [
                    {
                        "name": "delivery_time",
                        "type": "select",
                        "select": {
                            "soon": "Il prima possibile",
                            "1584228600": "19:30",
                            "1584230400": "20:00",
                            "1584232200": "20:30"
                        }
                    }
                ],
                "payment_methods": [
                    "paypal",
                    "contanti",
                    "stripe"
                ],
                "conditions": [
                    [
                        "total",
                        ">=",
                        "20"
                    ],
                    [
                        "distance",
                        "<",
                        "5000"
                    ]
                ]
            },
            {
                "id": "takeway",
                "name": "Ritiro in sede",
                "description": "Seleziona questa opzione per venire a ritirare il tuo ordine personalmente al nostro locale.",
                "price": "0",
                "fields": [],
                "payment_methods": [
                    "paypal",
                    "contanti",
                    "stripe"
                ],
                "conditions": []
            }
        ],
        "payment_methods": [
            {
                "id": "paypal",
                "description": "Paga con il tuo account PayPal",
                "settings": {
                    "paypal_account": "pay@contat.eu"
                }
            },
            {
                "id": "stripe",
                "name": "Paga con carta di credito",
                "settings": {
                    "public_key": "pk_live_E2T3oyTmtM3qOFWs3B1YNTAB",
                    "public_test_key": "pk_test_qDc3TC5rHG67vBlSdxUDrnDe"
                }
            },
            {
                "id": "contanti",
                "name": "Paga in contanti",
                "settings": []
            },
            {
                "id": "free",
                "name": "Pagamento non richiesto",
                "settings": []
            }
        ]
    },
    "status": "success"
}
 */