package eu.contat.jamesjoice.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import eu.contat.jamesjoice.model.order.ProductMetadataDeserializer ;

public class CartOrderProduct implements Parcelable {

    private String id, name, qty, total, subtotal;
    @JsonProperty("subtotal_tax")
    private String subtotalTax;
    @JsonProperty("total_tax")
    private String totalTax;
    @JsonProperty("meta_data")
  //  @JsonDeserialize(using = ProductMetadataDeserializer.class)
    private List<MetaData> metadata;

    public CartOrderProduct(){
        super();
    }

    protected CartOrderProduct(Parcel in) {
        id = in.readString();
        name = in.readString();
        qty = in.readString();
        total = in.readString();
        subtotal = in.readString();
        subtotalTax = in.readString();
        totalTax = in.readString();
        metadata = new ArrayList<>();
        in.readList(metadata, MetaData.class.getClassLoader());
    }

    public static final Creator<CartOrderProduct> CREATOR = new Creator<CartOrderProduct>() {
        @Override
        public CartOrderProduct createFromParcel(Parcel in) {
            return new CartOrderProduct(in);
        }

        @Override
        public CartOrderProduct[] newArray(int size) {
            return new CartOrderProduct[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(String subtotal) {
        this.subtotal = subtotal;
    }

    public String getSubtotalTax() {
        return subtotalTax;
    }

    public void setSubtotalTax(String subtotalTax) {
        this.subtotalTax = subtotalTax;
    }

    public String getTotalTax() {
        return totalTax;
    }

    public void setTotalTax(String totalTax) {
        this.totalTax = totalTax;
    }

    public List<MetaData> getMetadata() {
        return metadata;
    }

    public void setMetadata(List<MetaData> metadata) {
        this.metadata = metadata;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(name);
        parcel.writeString(qty);
        parcel.writeString(total);
        parcel.writeString(subtotal);
        parcel.writeString(subtotalTax);
        parcel.writeString(totalTax);
        parcel.writeList(metadata);
    }

    public static class MetaData implements Parcelable{

        private String key, name, value;

        protected MetaData(Parcel in) {
            key = in.readString();
            name = in.readString();
            value = in.readString();
        }

        public MetaData(){
            super();
        }

        public static final Creator<MetaData> CREATOR = new Creator<MetaData>() {
            @Override
            public MetaData createFromParcel(Parcel in) {
                return new MetaData(in);
            }

            @Override
            public MetaData[] newArray(int size) {
                return new MetaData[size];
            }
        };

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(key);
            parcel.writeString(name);
            parcel.writeString(value);
        }
    }
}
/*
{
                "id": "33",
                "name": "Primo prodotto",
                "qty": "1",
                "total": "90.00",
                "subtotal": "8.50",
                "subtotal_tax": "19.80",
                "total_tax": "19.80",
                "meta_data": [
                    {
                        "key": "Aggiunte selezionate",
                        "value": "Salame, formaggio"
                    }
                ]
            }
 */