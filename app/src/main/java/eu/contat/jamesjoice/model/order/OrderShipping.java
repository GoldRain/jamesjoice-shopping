package eu.contat.jamesjoice.model.order;

import android.os.Parcel;
import android.os.Parcelable;

public class OrderShipping implements Parcelable {

    private String method, latitude, longitude, accuracy, address, civic, city;

    public OrderShipping(){
        super();
    }

    protected OrderShipping(Parcel in) {
        method = in.readString();
        latitude = in.readString();
        longitude = in.readString();
        accuracy = in.readString();
        address = in.readString();
        civic = in.readString();
        city = in.readString();
    }

    public static final Creator<OrderShipping> CREATOR = new Creator<OrderShipping>() {
        @Override
        public OrderShipping createFromParcel(Parcel in) {
            return new OrderShipping(in);
        }

        @Override
        public OrderShipping[] newArray(int size) {
            return new OrderShipping[size];
        }
    };

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(String accuracy) {
        this.accuracy = accuracy;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCivic() {
        return civic;
    }

    public void setCivic(String civic) {
        this.civic = civic;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(method);
        parcel.writeString(latitude);
        parcel.writeString(longitude);
        parcel.writeString(accuracy);
        parcel.writeString(address);
        parcel.writeString(civic);
        parcel.writeString(city);
    }
}
/*
{
            "method": "delivery",
            "latitude": "42.042681",
            "longitude": "13.4327513",
            "accuracy": "30",
            "address": "Via dei Tulipani",
            "civic": "3C",
            "city": "Avezzano"
        }
 */