package eu.contat.jamesjoice.model.order;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class ProductMetadataDeserializer extends JsonDeserializer<Map<String, String>> {

    @Override
    public Map<String, String> deserialize(JsonParser jsonParser, DeserializationContext context) throws IOException, JsonProcessingException {
        JsonNode node = jsonParser.readValueAsTree();
        if (node.isArray()) {
            Map<String, String> map = new HashMap<>();
            for(Iterator<JsonNode> it = node.elements();  it.hasNext(); ){
                JsonNode n = it.next();
                if(n.isObject()) {
                    JsonNode key = n.findValue("key");
                    JsonNode value = n.findValue("value");
                    map.put(key.asText(), value.asText());
                }
            }
            return map;
        }

        return null;
    }

}
