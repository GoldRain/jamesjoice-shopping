package eu.contat.jamesjoice.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.Nullable;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Date;
import java.util.Objects;

public class Category implements Comparable<Category>, MenuEntity, Parcelable {

    private Long id;
    private String name;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date created, modified;
    private String permalink;
    private String content;
    @JsonIgnore
    private int order = -1;

    public Category() {
        super();
    }

    protected Category(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readLong();
        }
        name = in.readString();
        created = (java.util.Date) in.readSerializable();
        modified = (java.util.Date) in.readSerializable();
        permalink = in.readString();
        content = in.readString();
        order = in.readInt();
    }

    public static final Creator<Category> CREATOR = new Creator<Category>() {
        @Override
        public Category createFromParcel(Parcel in) {
            return new Category(in);
        }

        @Override
        public Category[] newArray(int size) {
            return new Category[size];
        }
    };

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    public String getPermalink() {
        return permalink;
    }

    public void setPermalink(String permaLink) {
        this.permalink = permaLink;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj != null && getClass().equals(obj.getClass())) {
            Category other = (Category) obj;
            return this.id != null && getId().equals(other.getId());
        } else {
            return false;
        }
    }

    @Override
    public int compareTo(Category category) {
        if (category != null) {
            if (this.order != -1 && category.getOrder() != -1) {
                return getOrder() < category.getOrder() ? -1 : equals(category) ? 0 : 1;
            } else {
                return this.order != -1 ? -1 : category.getOrder() != -1 ? 1 : 0;
            }
        } else {
            return -1;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        if (id == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeLong(id);
        }
        parcel.writeString(name);
        parcel.writeSerializable(created);
        parcel.writeSerializable(modified);
        parcel.writeString(permalink);
        parcel.writeString(content);
        parcel.writeInt(order);
    }
}

/*"id": "24",
        "name": "Alcolici",
        "created": "0000-00-00 00:00:00",
        "modified": "0000-00-00 00:00:00",
        "permalink": "alcolici",
        "content": ""
        */
