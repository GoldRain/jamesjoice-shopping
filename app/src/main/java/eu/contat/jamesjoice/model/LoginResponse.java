package eu.contat.jamesjoice.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class LoginResponse {

    private Long user_id;
    private String authtoken;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date login_date, last_time_used;
    private String device_type;
    private Long id;

    public Long getUser_id() {
        return user_id;
    }

    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }

    public String getAuthtoken() {
        return authtoken;
    }

    public void setAuthtoken(String authtoken) {
        this.authtoken = authtoken;
    }

    public Date getLogin_date() {
        return login_date;
    }

    public void setLogin_date(Date login_date) {
        this.login_date = login_date;
    }

    public Date getLast_time_used() {
        return last_time_used;
    }

    public void setLast_time_used(Date last_time_used) {
        this.last_time_used = last_time_used;
    }

    public String getDevice_type() {
        return device_type;
    }

    public void setDevice_type(String device_type) {
        this.device_type = device_type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
