package eu.contat.jamesjoice.model;

import android.util.Log;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

class CustomCategoryDeserializer extends JsonDeserializer<List<Long>> {

    @Override
    public List<Long> deserialize(JsonParser jsonParser, DeserializationContext context) throws IOException, JsonProcessingException {
        JsonNode node = jsonParser.readValueAsTree();
        if (node.isBoolean()) {
            return new ArrayList<>();
        }
        if (node.isArray()) {
            List<Long> list = new ArrayList<>();
            for (Iterator<JsonNode> it = node.elements(); it.hasNext(); ) {
                JsonNode e = it.next();
                try {
                    list.add(Long.parseLong(e.asText()));
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
            return list;
        }
        //   Log.d("deser", node.asText());
        return jsonParser.readValueAs(new TypeReference<List<Long>>() {
        });
    }

}
