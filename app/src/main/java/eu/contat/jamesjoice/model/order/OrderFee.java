package eu.contat.jamesjoice.model.order;

import android.os.Parcel;
import android.os.Parcelable;

public class OrderFee implements Parcelable {

    private String name, total;

    public OrderFee(){
        super();
    }

    protected OrderFee(Parcel in) {
        name = in.readString();
        total = in.readString();
    }

    public static final Creator<OrderFee> CREATOR = new Creator<OrderFee>() {
        @Override
        public OrderFee createFromParcel(Parcel in) {
            return new OrderFee(in);
        }

        @Override
        public OrderFee[] newArray(int size) {
            return new OrderFee[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        parcel.writeString(total);
    }
}

/*
 {
                "name": "Costo aggiuntivo",
                "total": "0.50"
            },
 */
