package eu.contat.jamesjoice.model;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import eu.contat.jamesjoice.JJApplication;

class CustomDeserializer extends JsonDeserializer<List<ServerImage>> {

    @Override
    public List<ServerImage> deserialize(JsonParser jsonParser, DeserializationContext context) throws IOException, JsonProcessingException {
        JsonNode node = jsonParser.readValueAsTree();
        if (node.isBoolean()) {
            return new ArrayList<>();
        }
        if (node.isArray()) {
            ObjectMapper jsonObjectMapper = JJApplication.getInstance().getObjectMapper();
            List<ServerImage> list = new ArrayList<>();
            for (Iterator<JsonNode> it = node.elements(); it.hasNext(); ) {
                JsonNode e = it.next();
                ServerImage img = jsonObjectMapper.treeToValue(e, ServerImage.class);
                list.add(img);
            }
            return list;
        }
        return jsonParser.readValueAs(new TypeReference<List<ServerImage>>() {
        });
    }

}
