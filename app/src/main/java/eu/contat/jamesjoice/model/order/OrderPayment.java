package eu.contat.jamesjoice.model.order;

import android.os.Parcel;
import android.os.Parcelable;

public class OrderPayment implements Parcelable {

    private String id, name, price;

    public OrderPayment(){
        super();
    }

    protected OrderPayment(Parcel in) {
        id = in.readString();
        name = in.readString();
        price = in.readString();
    }

    public static final Creator<OrderPayment> CREATOR = new Creator<OrderPayment>() {
        @Override
        public OrderPayment createFromParcel(Parcel in) {
            return new OrderPayment(in);
        }

        @Override
        public OrderPayment[] newArray(int size) {
            return new OrderPayment[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(name);
        parcel.writeString(price);
    }
}
