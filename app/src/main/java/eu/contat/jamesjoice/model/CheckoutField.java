package eu.contat.jamesjoice.model;

import androidx.annotation.Nullable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

public class CheckoutField {

    private String name, type,label,value;
    @JsonDeserialize(using = SettingsDeserializer.class)
    @JsonProperty("options")
    private Map<String, String> select;
    @JsonIgnore
    private Set<String> values = new HashSet<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Map<String, String> getSelect() {
        return select;
    }

    public void setSelect(Map<String, String> select) {
        this.select = select;
    }

    public Set<String> getValues() {
        return values;
    }

    public void setValues(Set<String> values) {
        this.values = values;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if(obj != null && getClass().equals(obj.getClass())){
            CheckoutField other = (CheckoutField) obj;
            return this.name != null && getName().equals(other.getName());
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.name);
    }
}
/*
 {
                    "name": "first_name",
                    "type": "text",
                    "label": "Nome",
                    "value": "user.meta.first_name"
                }
 */