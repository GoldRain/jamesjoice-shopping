package eu.contat.jamesjoice.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ServerImage implements Parcelable {

    @JsonDeserialize(using = ImageDeserializer.class)
    private Image thumb, medium, full;

    public ServerImage() {
        super();
    }

    protected ServerImage(Parcel in) {
        thumb = in.readParcelable(Image.class.getClassLoader());
        medium = in.readParcelable(Image.class.getClassLoader());
        full = in.readParcelable(Image.class.getClassLoader());
    }

    public static final Creator<ServerImage> CREATOR = new Creator<ServerImage>() {
        @Override
        public ServerImage createFromParcel(Parcel in) {
            return new ServerImage(in);
        }

        @Override
        public ServerImage[] newArray(int size) {
            return new ServerImage[size];
        }
    };

    public Image getThumb() {
        return thumb;
    }

    public void setThumb(Image thumb) {
        this.thumb = thumb;
    }

    public Image getMedium() {
        return medium;
    }

    public void setMedium(Image medium) {
        this.medium = medium;
    }

    public Image getFull() {
        return full;
    }

    public void setFull(Image full) {
        this.full = full;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(thumb, i);
        parcel.writeParcelable(medium, i);
        parcel.writeParcelable(full, i);
    }

    public static class Image implements Parcelable {

        private String url;
        private Integer height, width;

        public Image() {
            super();
        }

        protected Image(Parcel in) {
            url = in.readString();
            if (in.readByte() == 0) {
                height = null;
            } else {
                height = in.readInt();
            }
            if (in.readByte() == 0) {
                width = null;
            } else {
                width = in.readInt();
            }
        }

        public static final Creator<Image> CREATOR = new Creator<Image>() {
            @Override
            public Image createFromParcel(Parcel in) {
                return new Image(in);
            }

            @Override
            public Image[] newArray(int size) {
                return new Image[size];
            }
        };

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public Integer getHeight() {
            return height;
        }

        public void setHeight(Integer height) {
            this.height = height;
        }

        public Integer getWidth() {
            return width;
        }

        public void setWidth(Integer width) {
            this.width = width;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(url);
            if (height == null) {
                parcel.writeByte((byte) 0);
            } else {
                parcel.writeByte((byte) 1);
                parcel.writeInt(height);
            }
            if (width == null) {
                parcel.writeByte((byte) 0);
            } else {
                parcel.writeByte((byte) 1);
                parcel.writeInt(width);
            }
        }
    }
}
