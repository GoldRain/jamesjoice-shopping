package eu.contat.jamesjoice.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.Map;

public class PaymentMethod {

    private String id, description, name, currency;
    @JsonDeserialize(using = SettingsDeserializer.class)
    private Map<String, String> settings;
    @JsonProperty("client_secret")
    private String intent;
    @JsonProperty("server_total")
    private Double amount;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Map<String, String> getSettings() {
        return settings;
    }

    public void setSettings(Map<String, String> settings) {
        this.settings = settings;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIntent() {
        return intent;
    }

    public void setIntent(String intent) {
        this.intent = intent;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
/*
 {
                "id": "paypal",
                "description": "Paga con il tuo account PayPal",
                "settings": {
                    "paypal_account": "pay@contat.eu"
                }
            }
 */