package eu.contat.jamesjoice.model;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.List;

import eu.contat.jamesjoice.JJApplication;

public class ImageDeserializer extends JsonDeserializer<ServerImage.Image> {

    @Override
    public ServerImage.Image deserialize(JsonParser jsonParser, DeserializationContext context) throws IOException, JsonProcessingException {
        JsonNode node = jsonParser.readValueAsTree();
        if (node.isBoolean()) {
            return new ServerImage.Image();
        }
        if (node.isArray()) {
            if (node.size() >= 3) {
                ServerImage.Image img = new ServerImage.Image();
                img.setUrl(node.get(0).asText());
                if (node.get(1).isInt()) {
                    img.setHeight(node.get(1).asInt());
                }
                if (node.get(2).isInt()) {
                    img.setWidth(node.get(2).asInt());
                }
                return img;
            }
        }
        if (node.isObject()) {
            ObjectMapper mapper = JJApplication.getInstance().getObjectMapper();
            return mapper.treeToValue(node, ServerImage.Image.class);
        }
        return jsonParser.readValueAs(ServerImage.Image.class);
    }
}
