package eu.contat.jamesjoice;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import eu.contat.jamesjoice.model.Category;
import eu.contat.jamesjoice.model.JSendResponse;

public class MainActivity extends AppCompatActivity implements Response.Listener<String>, Response.ErrorListener {

    private Set<Category> mCategories = new HashSet<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onResponse(String response) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            JSendResponse<List<Category>> jsonResponse = mapper.readValue(response, new TypeReference<JSendResponse<List<Category>>>() {});
            if (jsonResponse.getStatus() != null && jsonResponse.getStatus().equals(JSendResponse.STATUS_success)) {
                if (jsonResponse.getData() != null) {
                    this.mCategories.clear();
                    this.mCategories.addAll(jsonResponse.getData());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
