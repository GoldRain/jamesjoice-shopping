package eu.contat.jamesjoice;

import android.app.Application;
import android.content.Context;
import android.graphics.Bitmap;

import androidx.collection.LruCache;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.stripe.android.PaymentConfiguration;

public class JJApplication extends Application {

    private static JJApplication instance;
    private RequestQueue requestQueue;
    private static Context ctx;
    private ImageLoader imageLoader;
    private ObjectMapper objectMapper;

    public JJApplication() {
        this.ctx = this;
        instance = this;
    }

    public static JJApplication getInstance() {
        if (instance == null) {
            throw new IllegalStateException("Application not initialized.");
        }
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        requestQueue = getRequestQueue();
     /*   PaymentConfiguration.init(
                getApplicationContext(),
                "pk_test_TYooMQauvdEDq54NiTphI7jx"
        );

      */
    }

    public RequestQueue getRequestQueue() {
        if (requestQueue == null) {
            // getApplicationContext() is key, it keeps you from leaking the
            // Activity or BroadcastReceiver if someone passes one in.
            requestQueue = Volley.newRequestQueue(ctx.getApplicationContext());
        }
        return requestQueue;
    }

    public ImageLoader getImageLoader() {
        getRequestQueue();
        if (imageLoader == null) {
            imageLoader = new ImageLoader(this.requestQueue, new LruBitmapCache());
        }

        return this.imageLoader;
    }

    public ObjectMapper getObjectMapper() {
        if (objectMapper == null) {
            objectMapper = new ObjectMapper();

            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        }
        return this.objectMapper;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        req.setTag(tag);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(String tag) {
        getRequestQueue().cancelAll(tag);
    }

    public static class LruBitmapCache extends LruCache<String, Bitmap> implements ImageLoader.ImageCache {

        public static int getDefaultLruCacheSize() {
            final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
            final int cacheSize = maxMemory / 8;

            return cacheSize;
        }

        public LruBitmapCache() {
            this(getDefaultLruCacheSize());
        }

        public LruBitmapCache(int maxSize) {
            super(maxSize);
        }

        @Override
        public Bitmap getBitmap(String url) {
            return get(url);
        }

        @Override
        public void putBitmap(String url, Bitmap bitmap) {
            put(url, bitmap);
        }
    }
}
