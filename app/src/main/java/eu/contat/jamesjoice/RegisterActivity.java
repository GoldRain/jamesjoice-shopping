package eu.contat.jamesjoice;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.material.textfield.TextInputLayout;

import java.io.IOException;

import eu.contat.jamesjoice.main.MenuActivity;
import eu.contat.jamesjoice.model.JSendResponse;
import eu.contat.jamesjoice.model.LoginResponse;
import eu.contat.jamesjoice.worker.LoginWorker;

public class RegisterActivity extends AppCompatActivity implements Response.Listener<String>, Response.ErrorListener {

    private EditText mMail, mName, mSurname, mPhone, mPassword, mRepeatPasswor;
    private String password, email;
    private TextInputLayout usernameTextObj;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        usernameTextObj = (TextInputLayout) findViewById(R.id.user);
        TextInputLayout passTextObj = (TextInputLayout) findViewById(R.id.password);
        TextInputLayout nomeTextObj = (TextInputLayout) findViewById(R.id.nome);
        TextInputLayout cognomeTextObj = (TextInputLayout) findViewById(R.id.cognome);
        TextInputLayout phoneextObj = (TextInputLayout) findViewById(R.id.phone);
        TextInputLayout rp = (TextInputLayout) findViewById(R.id.rp);
        Typeface font = Typeface.createFromAsset(getAssets(), "font/berlin_sans_fb_regular.ttf");
        usernameTextObj.setTypeface(font);
        passTextObj.setTypeface(font);
        nomeTextObj.setTypeface(font);
        cognomeTextObj.setTypeface(font);
        phoneextObj.setTypeface(font);
        rp.setTypeface(font);

        mMail = usernameTextObj.getEditText();
        mName = nomeTextObj.getEditText();
        mSurname = cognomeTextObj.getEditText();
        mPhone = phoneextObj.getEditText();
        mPassword = passTextObj.getEditText();
        mRepeatPasswor = rp.getEditText();

        TextView accedi = (TextView) findViewById(R.id.accedi);
        TextView forgot = (TextView) findViewById(R.id.forgot);

        accedi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(RegisterActivity.this, ForgotPasswordActivity.class));
            }
        });

        Button button = findViewById(R.id.buttonRegister);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validateForm()) {
                    email = mMail.getText().toString();
                    password = mPassword.getText().toString();
                    new LoginWorker().register(RegisterActivity.this,
                            RegisterActivity.this,
                            mMail.getText().toString(), mPassword.getText().toString(),
                            mRepeatPasswor.getText().toString(),
                            mPhone.getText().toString(),
                            mName.getText().toString(),
                            mSurname.getText().toString()
                    );
                }
            }
        });
    }

    private boolean validateForm() {
        if (mMail.getText().length() > 0 && mMail.getText() != null) {
            usernameTextObj.setErrorEnabled(false);
        } else {
            // usernameTextObj.setErrorEnabled(true);
            // usernameTextObj.setError("Campo obbligatorio.");

            return false;
        }
        String password = mPassword.getText() != null ? mPassword.getText().toString() : null;
        String repeatPassword = mRepeatPasswor.getText() != null ? mRepeatPasswor.getText().toString() : null;
        if (password != null && repeatPassword != null && password.equals(repeatPassword)) {

        } else {
            Toast.makeText(this, "passwords not matching", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(this, "Errore durante la registrazione.", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onResponse(String response) {
        ObjectMapper mapper = JJApplication.getInstance().getObjectMapper();
        try {
            JSendResponse jsonResponse = mapper.readValue(response, JSendResponse.class);
            if (jsonResponse.getStatus() != null && jsonResponse.getStatus().equals(JSendResponse.STATUS_success)) {
                new LoginWorker().login(new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        ObjectMapper mapper = JJApplication.getInstance().getObjectMapper();
                        try {
                            JSendResponse<LoginResponse> jsonResponse = mapper.readValue(response, new TypeReference<JSendResponse<LoginResponse>>() {
                            });
                            if (jsonResponse.getStatus() != null && jsonResponse.getStatus().equals(JSendResponse.STATUS_success)) {
                                startActivity(new Intent(RegisterActivity.this, MenuActivity.class));
                            } else if (jsonResponse.getMessage() != null) {
                                Toast.makeText(RegisterActivity.this, jsonResponse.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(RegisterActivity.this, "Errore di accesso.", Toast.LENGTH_LONG).show();
                        finish();
                    }
                }, email, password);
            } else if (jsonResponse.getMessage() != null) {
                Toast.makeText(this, jsonResponse.getMessage(), Toast.LENGTH_LONG).show();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
